﻿using System;
using System.Data.SqlClient;

namespace BotBMSTU
{
    partial class BotBMSTUDataSet 
    {
        private string CreateSurname(bool isMale)
        {
            char[] consonant = { 'б', 'в', 'г', 'к', 'л', 'м', 'п', 'р', 'ш', 'т' };
            char[] vowel = { 'а', 'о', 'у', 'е', 'ю', 'я', 'и' };
            string[] mEnding = { "ов", "ев", "ин" };
            string[] fEnding = { "ова", "ева", "ина" };
            Random r = new Random();
            string surname = "";
            for (int i = 0; i < 5; i++)
            {
                if (i % 2 == 0) surname += consonant[r.Next(0, consonant.Length - 1)];
                else surname += vowel[r.Next(0, vowel.Length - 1)];
            }
            if (isMale) surname += mEnding[r.Next(0, mEnding.Length - 1)];
            else surname += fEnding[r.Next(0, fEnding.Length - 1)];
            return surname;
        }

        private string ChooseName(bool isMale)
        {
            string[] mNames = { "иван", "дмитрий", "сергей", "никита", "антон", "кирилл", "илья", "фёдор", "василий", "константин" };
            string[] fNames = { "дарья", "екатерина", "юлия", "анастасия", "евгения", "ксения", "наталья", "валерия", "анна", "татьяна" };
            Random r = new Random();
            if (isMale) return mNames[r.Next(0, mNames.Length - 1)];
            else return fNames[r.Next(0, fNames.Length - 1)];
        }

        private int ChooseAge()
        {
            Random r = new Random();
            return r.Next(1960, 2002);
        }

        private string ChooseCity()
        {
            Random r = new Random();
            string[] cities = { "москва", "владивосток", "сыктывкар", "тверь", "Санкт-Петербург" };
            return cities[r.Next(0, cities.Length - 1)];
        }

        private string ChooseHobby()
        {
            Random r = new Random();
            string[] hobbies = { "музыка", "литература", "спорт", "путешествия", "программирование", "кулинария", "настольные игры" };
            return hobbies[r.Next(0, hobbies.Length - 1)];
        }

        private int GetMinId(string sex)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (sex == "ж") comm.CommandText = "SELECT MIN(vk_id) FROM FemaleUsers WHERE vk_id < 0";
                    else comm.CommandText = "SELECT MIN(vk_id) FROM MaleUsers WHERE vk_id < 0";
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    while (r.Read())
                    {
                        if (r[0] != DBNull.Value) return -1 * (int)r[0];
                        return 0;
                    }
                }
            }
            return 0;
        }
        public void FillUsersTables(string sex, int count)
        {
            int minId = GetMinId(sex) + 1;
            for (int i = 0; i < count; i++)
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    using (SqlCommand comm = conn.CreateCommand())
                    {
                        comm.CommandType = System.Data.CommandType.Text;

                        if (sex == "м")
                        {
                            comm.CommandText = "INSERT INTO MaleUsers VALUES (@surname, @name, @age, @city, @inLove, NULL, @vk_id, NULL)";
                            comm.Parameters.AddWithValue("@surname", CreateSurname(true));
                            comm.Parameters.AddWithValue("@name", ChooseName(true));
                        }
                        else
                        {
                            comm.CommandText = "INSERT INTO FemaleUsers VALUES (@surname, @name, @age, @city, @inLove, NULL, @vk_id, NULL)";
                            comm.Parameters.AddWithValue("@surname", CreateSurname(false));
                            comm.Parameters.AddWithValue("@name", ChooseName(false));
                        }
                        comm.Parameters.AddWithValue("@age", ChooseAge());
                        comm.Parameters.AddWithValue("@city", ChooseCity());
                        comm.Parameters.AddWithValue("@inLove", 0);
                        comm.Parameters.AddWithValue("@vk_id", -1 * (i + minId));
                        conn.Open();
                        comm.ExecuteNonQuery();
                        conn.Close();
                        AddHobbies(-1 * (i + minId), ChooseHobby(), sex);
                        int min = ChooseAge();
                        int max = ChooseAge();
                        AddAgeRequests(min, max, sex, -1 * (i + minId));
                        string[] data = { ChooseCity() };
                        AddCityRequests(data, sex, -1 * (i + minId));
                        data[0] = ChooseHobby();
                        AddHobbyRequests(data, sex, -1 * (i + minId));
                    }
                }
            }
        }
    }
}