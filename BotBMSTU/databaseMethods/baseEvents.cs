﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace BotBMSTU
{
    partial class BotBMSTUDataSet
    {
        public void DeleteEventByName(string name)
        {
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "DELETE FROM Events WHERE name = @name";
                    comm.Parameters.AddWithValue("@name", name);
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }

        public List<string> GetEvents()
        {
            List<string> events = new List<string>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "SELECT name, description, date FROM Events ORDER BY date";
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    if (r.HasRows)
                    {
                        while (r.Read())
                        {
                            events.Add(((Convert.ToDateTime(r[2]).Date).ToString().Split(' '))[0] + " - " + ChangeFirstLetter(r[0].ToString()) + " - " + ChangeFirstLetter(r[1].ToString()));
                        }
                    }
                }
                return events;
            }
        }

        public bool IsNameOfEvent(string name)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "SELECT * FROM Events WHERE name = @name";
                    comm.Parameters.AddWithValue("@name", name);
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    if (r.HasRows) return true;
                    return false;
                }
            }
        }

        public void EditEvent(string parametr, string value, string name)
        {
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    switch (parametr)
                    {
                        case "название":
                            comm.CommandText = "UPDATE Events SET name = @value WHERE name = @name";
                            comm.Parameters.AddWithValue("@value", value);
                            break;
                        case "описание":
                            comm.CommandText = "UPDATE Events SET description = @value WHERE name = @name";
                            comm.Parameters.AddWithValue("@value", value);
                            break;
                        case "дату":
                            comm.CommandText = "UPDATE Events SET date = @value WHERE name = @name";
                            comm.Parameters.AddWithValue("@value", GetDateFromString(value));
                            break;
                    }
                    comm.Parameters.AddWithValue("@name", name);
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }

        public void AddEvent(string name, string description, string date)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "INSERT INTO Events VALUES(@name, @description, @date)";
                    comm.Parameters.AddWithValue("@name", name);
                    comm.Parameters.AddWithValue("@description", description);
                    comm.Parameters.AddWithValue("@date", GetDateFromString(date));
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }
    }
}