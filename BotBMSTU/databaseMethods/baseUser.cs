﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;

namespace BotBMSTU
{
    partial class BotBMSTUDataSet
    {
        public bool IsUser(long? vk_id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;

                    comm.CommandText = "SELECT * FROM MaleUsers WHERE vk_id = @vk_id";
                    comm.Parameters.AddWithValue("@vk_id", vk_id);
                    conn.Open();
                    SqlDataReader reader = comm.ExecuteReader();
                    if (reader.HasRows) return true;
                    conn.Close();
                    comm.CommandText = "SELECT * FROM FemaleUsers WHERE vk_id = @vk_id";
                    conn.Open();
                    reader = comm.ExecuteReader();
                    if (reader.HasRows) return true;
                    return false;
                }
            }
        }

        public void AddUser(long? id, string surname, string name, string city, int age, string sex, VkNet.Model.Message message)
        {
            string path = null;
            foreach (var k in message.Attachments)
            {
                if (k.Type == typeof(VkNet.Model.Attachments.Photo))
                {
                    WebClient wb = new WebClient();
                    path = Environment.CurrentDirectory.Remove(Environment.CurrentDirectory.IndexOf("BotBMSTU")) + "BotBMSTU\\BotBMSTU\\assets\\bot.jpg";
                    var photo = k.Instance as VkNet.Model.Attachments.Photo;
                    wb.DownloadFile(photo.Sizes[photo.Sizes.Count - 1].Url, path);
                    break;
                }
            }
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (path == null)
                    {
                        if (sex == "ж") comm.CommandText = "INSERT INTO FemaleUsers VALUES (@surname, @name, @age, @city, 0, NULL, @vk_id, NULL)";
                        else comm.CommandText = "INSERT INTO MaleUsers VALUES (@surname, @name, @age, @city, 0, NULL, @vk_id, NULL)";
                    }
                    else
                    {
                        if (sex == "ж") comm.CommandText = "INSERT INTO FemaleUsers VALUES (@surname, @name, @age, @city, 0, @photo, @vk_id, @format)";
                        else comm.CommandText = "INSERT INTO MaleUsers VALUES (@surname, @name, @age, @city, 0, @photo, @vk_id, @format)";
                        comm.Parameters.AddWithValue("@photo", ConvertPhotoInBytes(path));
                        comm.Parameters.AddWithValue("@format", GetPhotoFormat(path));
                    }
                    comm.Parameters.AddWithValue("@surname", surname);
                    comm.Parameters.AddWithValue("@name", name);
                    comm.Parameters.AddWithValue("age", age);
                    comm.Parameters.AddWithValue("@city", city);
                    comm.Parameters.AddWithValue("@vk_id", id);
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }

        public void AddHobbies(long? id, string hobbies, string sex)
        {
            string[] result;
            try
            {
                result = hobbies.Split(',');
            }
            catch
            {
                result = new string[1];
                result[0] = hobbies.Trim();
            }
            for (int i = 0; i < result.Length; i++) result[i] = result[i].Trim();
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (sex == "ж") comm.CommandText = "INSERT INTO Female_hobby VALUES ((SELECT id FROM FemaleUsers WHERE vk_id = @vk_id), @name)";
                    else comm.CommandText = "INSERT INTO Male_hobby VALUES ((SELECT id FROM MaleUsers WHERE vk_id = @vk_id), @name)";
                    comm.Parameters.AddWithValue("@vk_id", id);
                    foreach (string r in result)
                    {
                        comm.Parameters.Clear();
                        comm.Parameters.AddWithValue("@vk_id", id);
                        comm.Parameters.AddWithValue("@name", r);
                        conn.Open();
                        comm.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
        }

        public List<string> GetInformationById(long? id, string sex)
        {
            List<string> result = new List<string>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (sex == "м") comm.CommandText = "SELECT * From MaleUsers WHERE vk_id = @id";
                    else comm.CommandText = "SELECT * From FemaleUsers WHERE vk_id = @id";
                    comm.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    if (r.HasRows)
                    {
                        while (r.Read())
                        {
                            result.Add(ChangeFirstLetter(r[1].ToString()));
                            result.Add(ChangeFirstLetter(r[2].ToString()));
                            result.Add(r[3].ToString());
                            result.Add(ChangeFirstLetter(r[4].ToString()));
                            result.Add(sex);
                            if (r[6] != DBNull.Value) result.Add("true");
                            return result;
                        }
                    }
                }
            }
            return null;
        }

        public void DeleteUserById(long? id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "DELETE From FemaleUsers WHERE vk_id = @id";
                    comm.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                    comm.CommandText = "DELETE From MaleUsers WHERE vk_id = @id";
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }

        public string GetPhotoPathById(long? id, string sex)
        {
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (sex == "ж") comm.CommandText = "SELECT photo, photo_format From FemaleUsers WHERE vk_id = @id";
                    else comm.CommandText = "SELECT photo, photo_format From MaleUsers WHERE vk_id = @id";
                    comm.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    if (r.HasRows)
                    {
                        while (r.Read())
                        {
                            if (r[0] != DBNull.Value) return ConvertBytesInPhoto((byte[])r[0], r[1].ToString());
                        }
                    }
                    return null;
                }
            }
        }

        public void UpdateUserById(int parametr, string value, long? id, string sex, VkNet.Model.Message message)
        {
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    switch (parametr)
                    {
                        case 0:
                            if (sex == "ж") comm.CommandText = "UPDATE FemaleUsers SET surname = @surname WHERE vk_id = @id";
                            else comm.CommandText = "UPDATE MaleUsers SET surname = @surname WHERE vk_id = @id";
                            comm.Parameters.AddWithValue("@surname", value);
                            break;
                        case 1:
                            if (sex == "ж") comm.CommandText = "UPDATE FemaleUsers SET name = @name WHERE vk_id = @id";
                            else comm.CommandText = "UPDATE MaleUsers SET name = @name WHERE vk_id = @id";
                            comm.Parameters.AddWithValue("@name", value);
                            break;
                        case 2:
                            if (sex == "ж") comm.CommandText = "UPDATE FemaleUsers SET age = @age WHERE vk_id = @id";
                            else comm.CommandText = "UPDATE MaleUsers SET age = @age WHERE vk_id = @id";
                            comm.Parameters.AddWithValue("@age", value);
                            break;
                        case 3:
                            if (sex == "ж") comm.CommandText = "UPDATE FemaleUsers SET city = @city WHERE vk_id = @id";
                            else comm.CommandText = "UPDATE MaleUsers SET city = @city WHERE vk_id = @id";
                            comm.Parameters.AddWithValue("@city", value);
                            break;
                        case 4:
                            string path = null;
                            foreach (var k in message.Attachments)
                            {
                                if (k.Type == typeof(VkNet.Model.Attachments.Photo))
                                {
                                    WebClient wb = new WebClient();
                                    path = Environment.CurrentDirectory.Remove(Environment.CurrentDirectory.IndexOf("BotBMSTU")) + "BotBMSTU\\BotBMSTU\\assets\\bot.jpg";
                                    var photo = k.Instance as VkNet.Model.Attachments.Photo;
                                    wb.DownloadFile(photo.Sizes[photo.Sizes.Count - 1].Url, path);
                                    break;
                                }
                            }
                            if (path == null)
                            {
                                if (sex == "ж") comm.CommandText = "UPDATE FemaleUsers SET photo = NULL, photo_format = NULL WHERE vk_id = @id";
                                else comm.CommandText = "UPDATE MaleUsers SET photo = NULL, photo_format = NULL WHERE vk_id = @id";
                            }
                            else
                            {
                                if (sex == "ж") comm.CommandText = "UPDATE FemaleUsers SET photo = @photo, photo_format = @format WHERE vk_id = @id";
                                else comm.CommandText = "UPDATE MaleUsers SET photo = @photo, photo_format = @format WHERE vk_id = @id";
                                comm.Parameters.AddWithValue("@photo", ConvertPhotoInBytes(path));
                                comm.Parameters.AddWithValue("@format", GetPhotoFormat(path));
                            }
                            break;
                    }
                    comm.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }

        public void DeleteHobbyById(long? id, string sex)
        {
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (sex == "ж") comm.CommandText = "DELETE FROM Female_hobby WHERE id_user = (SELECT id FROM FemaleUsers WHERE vk_id = @id)";
                    else comm.CommandText = "DELETE FROM Male_hobby WHERE id_user = (SELECT id FROM MaleUsers WHERE vk_id = @id)";
                    comm.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }

        public void ChangeStatusById(long? id, bool status, string sex)
        {
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (sex == "ж") comm.CommandText = "UPDATE FemaleUsers SET inLove = @status WHERE vk_id = @id";
                    else comm.CommandText = "UPDATE MaleUsers SET inLove = @status WHERE vk_id = @id";
                    if (status) comm.Parameters.AddWithValue("@status", "true");
                    else comm.Parameters.AddWithValue("@status", "false");
                    comm.Parameters.AddWithValue("@id", id);
                    conn.Close();
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }

        public string GetSexById(long? id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "SELECT * FROM MaleUsers WHERE vk_id = @id";
                    comm.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    if (r.HasRows) return "м";
                    return "ж";
                }
            }
        }

        public string GetNameSurnameById(long? id)
        {
            string sex = GetSexById(id);
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (sex == "ж") comm.CommandText = "SELECT surname, name FROM FemaleUsers WHERE vk_id = @id";
                    else comm.CommandText = "SELECT surname, name FROM MaleUsers WHERE vk_id = @id";
                    comm.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    while (r.Read())
                    {
                        return ChangeFirstLetter(r[0].ToString()) + " " + ChangeFirstLetter(r[1].ToString());
                    }
                }
                return "";
            }
        }

        public int GetUserStatistics(string sex)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (sex == "ж") comm.CommandText = "SELECT COUNT(*) FROM FemaleUsers";
                    else comm.CommandText = "SELECT COUNT(*) FROM MaleUsers";
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    while (r.Read()) return Convert.ToInt32(r[0]);
                }
                return 0;
            }
        }

        public List<int> GetIdOfCandidates(long? id, string sex)
        {
            List<int> result = new List<int>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (sex == "м") comm.CommandText = "SELECT vk_id FROM FemaleUsers WHERE inLove = 0 AND id IN" +
                                                       "(SELECT id_user FROM Female_Hobby WHERE name IN" +
                                                       "(SELECT hobby FROM MaleHobbyRequests WHERE id_user =" + 
                                                       "(SELECT id FROM MaleUsers WHERE vk_id = @vk_id)) OR " + 
                                                       "N'любое' IN (SELECT hobby FROM MaleHobbyRequests WHERE id_user =" +
                                                       "(SELECT id FROM MaleUsers WHERE vk_id = @vk_id))) AND id IN" +
                                                       "(SELECT id FROM FemaleUsers WHERE city IN" +
                                                       "(SELECT city FROM MaleCityRequests WHERE id_user =" +
                                                       "(SELECT id FROM MaleUsers WHERE vk_id = @vk_id)) OR " + 
                                                       "N'любой' IN (SELECT city FROM MaleCityRequests WHERE id_user =" + 
                                                       "(SELECT id FROM MaleUsers WHERE vk_id = @vk_id))) AND id IN" + 
                                                       "(SELECT id FROM FemaleUsers WHERE age >=" +
                                                       "(SELECT MIN(age) FROM MaleAgeRequests WHERE id_user =" +
                                                       "(SELECT id FROM MaleUsers WHERE vk_id = @vk_id))) AND id IN" +
                                                       "(SELECT id FROM FemaleUsers WHERE age <=" + 
                                                       "(SELECT MAX(age) FROM MaleAgeRequests WHERE id_user =" +
                                                       "(SELECT id FROM MaleUsers WHERE vk_id = @vk_id)))";
                    else comm.CommandText = "SELECT vk_id FROM MaleUsers WHERE inLove = 0 AND id IN" +
                                            "(SELECT id_user FROM Male_Hobby WHERE name IN" +
                                            "(SELECT hobby FROM FemaleHobbyRequests WHERE id_user =" +
                                            "(SELECT id FROM FemaleUsers WHERE vk_id = @vk_id)) OR " + 
                                            "N'любое' IN (SELECT hobby FROM FemaleHobbyRequests WHERE id_user =" + 
                                            "(SELECT id FROM FemaleUsers WHERE vk_id = @vk_id))) AND id IN" +
                                            "(SELECT id FROM MaleUsers WHERE city IN" +
                                            "(SELECT city FROM FemaleCityRequests WHERE id_user =" + 
                                            "(SELECT id FROM FemaleUsers WHERE vk_id = @vk_id)) OR " +
                                            "N'любой' IN (SELECT city FROM FemaleCityRequests WHERE id_user =" + 
                                            "(SELECT id FROM FemaleUsers WHERE vk_id = @vk_id))) AND id IN" +
                                            "(SELECT id FROM MaleUsers WHERE age >=" +
                                            "(SELECT MIN(age) FROM FemaleAgeRequests WHERE id_user =" +
                                            "(SELECT id FROM FemaleUsers WHERE vk_id = @vk_id))) AND id IN" +
                                            "(SELECT id FROM MaleUsers WHERE age <=" +
                                            "(SELECT MAX(age) FROM FemaleAgeRequests WHERE id_user =" +
                                            "(SELECT id FROM FemaleUsers WHERE vk_id = @vk_id)))";
                    comm.Parameters.AddWithValue("@vk_id", id);
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    while (r.Read())
                    {
                        result.Add((int)r[0]);
                    }
                }
            }
            return result;
        }

        public int GetVkId(int id, string sex)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (sex == "ж") comm.CommandText = "SELECT vk_id FROM FemaleUsers WHERE id = @id";
                    else comm.CommandText = "SELECT vk_id FROM MaleUsers WHERE id = @id";
                    comm.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    while (r.Read()) return (int)r[0];
                }
                return 0;
            }
        }
    }
}