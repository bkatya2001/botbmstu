﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace BotBMSTU
{
    partial class BotBMSTUDataSet
    {
        public void DeletePairByMaleUserId(long? id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "DELETE FROM Pairs WHERE id_male = @id";
                    comm.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }

        public List<string> SelectIdInPair(long? id)
        {
            List<string> result = new List<string>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "SELECT id_male, id_female FROM Pairs WHERE id_male = (SELECT id FROM MaleUsers WHERE vk_id = @id) OR id_female = (SELECT id FROM FemaleUsers WHERE vk_id = @id)";
                    comm.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    while (r.Read())
                    {
                        result.Add(r[0].ToString());
                        result.Add(r[1].ToString());
                    }
                }
                return result;
            }
        }

        public void ChangePair(long? oldId, long? newId, string sex, DateTime date)
        {
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (sex == "ж") comm.CommandText = "UPDATE Pairs SET date = @date, id_female = (SELECT id FROM FemaleUsers WHERE vk_id = @newId) WHERE id_female = (SELECT id FROM FemaleUsers WHERE vk_id = @oldId)";
                    else comm.CommandText = "UPDATE Pairs SET date = @date, id_male = (SELECT id FROM MaleUsers WHERE vk_id = @newId) WHERE id_male = (SELECT id FROM MaleUsers WHERE vk_id = @oldId)";
                    comm.Parameters.AddWithValue("@newId", newId);
                    comm.Parameters.AddWithValue("@oldId", oldId);
                    comm.Parameters.AddWithValue("@date", date);
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }

        public string AddPair(long? id1, long? id2)
        {
            string sex1 = GetSexById(id1);
            string sex2 = GetSexById(id2);
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (sex1 == sex2) return "Нельзя создать однополую пару";
                    else
                    {
                        comm.CommandText = "INSERT INTO Pairs VALUES((SELECT id FROM MaleUsers WHERE vk_id = @male), (SELECT id FROM FemaleUsers WHERE vk_id = @Female), @date)";
                        if (sex1 == "ж")
                        {
                            comm.Parameters.AddWithValue("@male", id2);
                            comm.Parameters.AddWithValue("@female", id1);
                        }
                        else
                        {
                            comm.Parameters.AddWithValue("@male", id1);
                            comm.Parameters.AddWithValue("@female", id2);
                        }
                    }
                    comm.Parameters.AddWithValue("@date", DateTime.Now.Date);
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
            return "Пара создана!";
        }

        public int GetPairStatistics(string minDate, string maxDate)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "SELECT COUNT(*) FROM Pairs WHERE date <= @maxDate AND date >= @minDate";
                    comm.Parameters.AddWithValue("@maxDate", GetDateFromString(maxDate));
                    comm.Parameters.AddWithValue("@minDate", GetDateFromString(minDate));
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    while (r.Read()) return Convert.ToInt32(r[0]);
                }
            }
            return 0;
        }

        public int GetPairStatistics()
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "SELECT COUNT(*) FROM Pairs";
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    while (r.Read()) return Convert.ToInt32(r[0]);
                }
                return 0;
            }
        }
    }
}