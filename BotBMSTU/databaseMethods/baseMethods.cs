﻿using System;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Data.SqlClient;

namespace BotBMSTU
{
    partial class BotBMSTUDataSet
    {
        private string connectionString = "Data Source=" + Environment.MachineName + "\\SQLEXPRESS;Initial Catalog=BotBMSTU;Integrated Security=True";
        private byte[] ConvertPhotoInBytes(string path)
        {
            byte[] imageData = null;
            FileInfo fInfo = new FileInfo(path);
            long numBytes = fInfo.Length;
            FileStream fStream = new FileStream(path, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fStream);
            imageData = br.ReadBytes((int)numBytes);
            return imageData;
        }

        private string GetPhotoFormat(string path)
        {
            return (Path.GetExtension(path)).Replace(".", "").ToLower();
        }

        private string ConvertBytesInPhoto(byte[] bytes, string format)
        {
            MemoryStream ms = new MemoryStream(bytes);
            Image newImage = Image.FromStream(ms);
            string path = Environment.CurrentDirectory.Remove(Environment.CurrentDirectory.IndexOf("BotBMSTU")) + "BotBMSTU\\BotBMSTU\\assets\\bot." + format;
            if (format == "png") { newImage.Save(path, System.Drawing.Imaging.ImageFormat.Png); }
            else if (format == "jpg" || format == "jpeg") { newImage.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg); }
            else if (format == "gif") { newImage.Save(path, System.Drawing.Imaging.ImageFormat.Gif); }
            return path;
        }

        private DateTime GetDateFromString(string s)
        {
            string format = "dd.MM.yyyy";
            Regex d = new Regex(@"\d{2}.\d{2}.\d{4}");
            if (d.IsMatch(s))
            {
                try
                {
                    return DateTime.ParseExact(d.Match(s).Value, format, CultureInfo.InvariantCulture).Date;
                }
                catch { return DateTime.Now.Date; }
            }
            return DateTime.Now.Date;
        }

        public void ClearBase()
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "DELETE FROM Events WHERE date < @date";
                    comm.Parameters.AddWithValue("@date", DateTime.Now.Date);
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }

        public string ChangeFirstLetter(string text)
        {
            return text[0].ToString().ToUpper() + text.Remove(0, 1);
        }

        public void CreateDatabase()
        {
            int result = 1;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "SELECT COUNT(name) FROM sysdatabases WHERE [name] = 'BotBMSTU'";
                    conn.Open();
                    result = (int)comm.ExecuteScalar();
                    conn.Close();
                    if (result == 0)
                    {
                        comm.CommandText = "CREATE DATABASE [BotBMSTU]";
                        conn.Open();
                        comm.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            if (result == 0) CreateTables();
        }

        public void CreateTables()
        {
            string[] tables = { "CREATE TABLE [dbo].[Admins] ([id] [int] IDENTITY(1,1) NOT NULL, [vk_id] [int] NULL, PRIMARY KEY CLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]",
                "CREATE TABLE [dbo].[Events] ([id] [int] IDENTITY(1,1) NOT NULL, [name] [nvarchar] (max) NULL, [description] [nvarchar] (max) NULL, [date] [date] NULL, PRIMARY KEY CLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]",
                "CREATE TABLE [dbo].[FemaleUsers] ([id] [int] IDENTITY(1,1) NOT NULL, [surname] [nvarchar] (max) NULL, [name] [nvarchar] (max) NULL, [age] [int] NULL, [city] [nvarchar] (max) NULL, [inLove] [bit] NULL, [photo] [image] NULL, [vk_id] [int] NULL, [photo_format] [nvarchar] (max) NULL, PRIMARY KEY CLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]",
                "CREATE TABLE [dbo].[MaleUsers] ([id] [int] IDENTITY(1,1) NOT NULL, [surname] [nvarchar] (max) NULL, [name] [nvarchar] (max )NULL, [age] [int] NULL, [city] [nvarchar] (max) NULL, [inLove] [bit] NULL, [photo] [image] NULL, [vk_id] [int] NULL, [photo_format] [nvarchar] (max) NULL, PRIMARY KEY CLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]",
                "CREATE TABLE [dbo].[Female_Hobby] ([id] [int] IDENTITY(1,1) NOT NULL, [id_user] [int] NULL, [name] [nvarchar] (max) NULL, PRIMARY KEY CLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]",
                "CREATE TABLE [dbo].[FemaleAgeRequests] ([id] [int] IDENTITY(1,1) NOT NULL, [id_user] [int] NULL, [age] [int] NULL, PRIMARY KEY CLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]",
                "CREATE TABLE [dbo].[FemaleCityRequests] ([id] [int] IDENTITY(1,1) NOT NULL, [id_user] [int] NULL, [city] [nvarchar] (max) NULL, PRIMARY KEY CLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]",
                "CREATE TABLE [dbo].[FemaleHobbyRequests] ([id] [int] IDENTITY(1,1) NOT NULL, [id_user] [int] NULL, [hobby] [nvarchar] (max) NULL, PRIMARY KEY CLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]",
                "CREATE TABLE [dbo].[Male_Hobby] ([id] [int] IDENTITY(1,1) NOT NULL, [id_user] [int] NULL, [name] [nvarchar] (max) NULL, PRIMARY KEY CLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]",
                "CREATE TABLE [dbo].[MaleAgeRequests] ([id] [int] IDENTITY(1,1) NOT NULL, [id_user] [int] NULL, [age] [int] NULL, PRIMARY KEY CLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]",
                "CREATE TABLE [dbo].[MaleCityRequests] ([id] [int] IDENTITY(1,1) NOT NULL, [id_user] [int] NULL, [city] [nvarchar] (max) NULL, PRIMARY KEY CLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]",
                "CREATE TABLE [dbo].[MaleHobbyRequests] ([id] [int] IDENTITY(1,1) NOT NULL, [id_user] [int] NULL, [hobby] [nvarchar] (max) NULL, PRIMARY KEY CLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]",
                "CREATE TABLE [dbo].[Pairs] ([id] [int] IDENTITY(1,1) NOT NULL, [id_male] [int] NULL, [id_female] [int] NULL, [date] [date] NULL, PRIMARY KEY CLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]" };
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    foreach (var t in tables)
                    {
                        comm.CommandText = t;
                        conn.Open();
                        comm.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
        }
    }
}