﻿using System.Data.SqlClient;

namespace BotBMSTU
{
    partial class BotBMSTUDataSet
    {
        public bool IsAdmin(long? vk_id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;

                    comm.CommandText = "SELECT id FROM Admins WHERE vk_id = @vk_id";
                    comm.Parameters.AddWithValue("@vk_id", vk_id);
                    conn.Open();
                    SqlDataReader reader = comm.ExecuteReader();
                    if (reader.HasRows) return true;
                    return false;
                }
            }
        }

        public void Add(long? vk_id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;

                    comm.CommandText = "INSERT INTO Admins VALUES (@vk_id)";
                    comm.Parameters.AddWithValue("@vk_id", vk_id);
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }

        public void DeleteAdminById(long? vk_id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "DELETE FROM Admins WHERE vk_id = @vk_id";
                    comm.Parameters.AddWithValue("@vk_id", vk_id);
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }
    }
}