﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace BotBMSTU
{
    partial class BotBMSTUDataSet
    {
        public void AddAgeRequests(int min, int max, string sex, long? id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (sex == "ж") comm.CommandText = "INSERT INTO FemaleAgeRequests VALUES((SELECT id FROM FemaleUsers WHERE vk_id = @id), @age)";
                    else comm.CommandText = "INSERT INTO MaleAgeRequests VALUES((SELECT id FROM MaleUsers WHERE vk_id = @id), @age)";
                    comm.Parameters.AddWithValue("@id", id);
                    comm.Parameters.AddWithValue("@age", min);
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                    comm.Parameters.Clear();
                    comm.Parameters.AddWithValue("@id", id);
                    comm.Parameters.AddWithValue("@age", max);
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }

        public void AddHobbyRequests(string[] hobbies, string sex, long? id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (sex == "ж") comm.CommandText = "INSERT INTO FemaleHobbyRequests VALUES((SELECT id FROM FemaleUsers WHERE vk_id = @id), @hobby)";
                    else comm.CommandText = "INSERT INTO MaleHobbyRequests VALUES((SELECT id FROM MaleUsers WHERE vk_id = @id), @hobby)";
                    for (int i = 0; i < hobbies.Length; i++)
                    {
                        hobbies[i] = hobbies[i].Trim();
                        comm.Parameters.AddWithValue("@id", id);
                        comm.Parameters.AddWithValue("@hobby", hobbies[i]);
                        conn.Open();
                        comm.ExecuteNonQuery();
                        conn.Close();
                        comm.Parameters.Clear();
                    }
                }
            }
        }

        public void AddCityRequests(string[] cities, string sex, long? id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (sex == "ж") comm.CommandText = "INSERT INTO FemaleCityRequests VALUES((SELECT id FROM FemaleUsers WHERE vk_id = @id), @city)";
                    else comm.CommandText = "INSERT INTO MaleCityRequests VALUES((SELECT id FROM MaleUsers WHERE vk_id = @id), @city)";
                    for (int i = 0; i < cities.Length; i++)
                    {
                        cities[i] = cities[i].Trim();
                        comm.Parameters.AddWithValue("@id", id);
                        comm.Parameters.AddWithValue("@city", cities[i]);
                        conn.Open();
                        comm.ExecuteNonQuery();
                        conn.Close();
                        comm.Parameters.Clear();
                    }
                }
            }
        }

        public List<string> GetHobbyRequestsById(long? id, string sex)
        {
            List<string> result = new List<string>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (sex == "ж") comm.CommandText = "SELECT hobby FROM FemaleHobbyRequests WHERE id_user = (SELECT id FROM FemaleUsers WHERE vk_id = @id)";
                    else comm.CommandText = "SELECT hobby FROM MaleHobbyRequests WHERE id_user = (SELECT id FROM MaleUsers WHERE vk_id = @id)";
                    comm.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    while (r.Read())
                    {
                        result.Add(r[0].ToString());
                    }
                }
                return result;
            }
        }

        public string GetAgeRangeById(long? id, string sex)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (sex == "ж") comm.CommandText = "SELECT MIN(age), MAX(age) FROM FemaleAgeRequests WHERE id_user = (SELECT id FROM FemaleUsers WHERE vk_id = @id)";
                    else comm.CommandText = "SELECT MIN(age), MAX(age) FROM MaleAgeRequests WHERE id_user = (SELECT id FROM MaleUsers WHERE vk_id = @id)";
                    comm.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    while (r.Read())
                    {
                        return (DateTime.Now.Year - Convert.ToInt32(r[1])).ToString() + "-" + (DateTime.Now.Year - Convert.ToInt32(r[0])).ToString();
                    }
                }
                return "1-100";
            }
        }

        public List<string> GetCityRequestsById(long? id, string sex)
        {
            List<string> result = new List<string>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (sex == "ж") comm.CommandText = "SELECT city FROM FemaleCityRequests WHERE id_user = (SELECT id FROM FemaleUsers WHERE vk_id = @id)";
                    else comm.CommandText = "SELECT city FROM MaleCityRequests WHERE id_user = (SELECT id FROM MaleUsers WHERE vk_id = @id)";
                    comm.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    while (r.Read())
                    {
                        result.Add(ChangeFirstLetter(r[0].ToString()));
                    }
                }
                return result;
            }
        }

        public void DeleteAgeRequestsById(long? id, string sex)
        {
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (sex == "ж") comm.CommandText = "DELETE FROM FemaleAgeRequests WHERE id_user = (SELECT id FROM FemaleUsers WHERE vk_id = @id)";
                    else comm.CommandText = "DELETE FROM MaleAgeRequests WHERE id_user = (SELECT id FROM MaleUsers WHERE vk_id = @id)";
                    comm.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }

        public void DeleteCityRequestsById(long? id, string sex)
        {
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (sex == "ж") comm.CommandText = "DELETE FROM FemaleCityRequests WHERE id_user = (SELECT id FROM FemaleUsers WHERE vk_id = @id)";
                    else comm.CommandText = "DELETE FROM MaleCityRequests WHERE id_user = (SELECT id FROM MaleUsers WHERE vk_id = @id)";
                    comm.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }

        public void DeleteHobbyRequestsById(long? id, string sex)
        {
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    if (sex == "ж") comm.CommandText = "DELETE FROM FemaleHobbyRequests WHERE id_user = (SELECT id FROM FemaleUsers WHERE vk_id = @id)";
                    else comm.CommandText = "DELETE FROM MaleHobbyRequests WHERE id_user = (SELECT id FROM MaleUsers WHERE vk_id = @id)";
                    comm.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }
    }
}