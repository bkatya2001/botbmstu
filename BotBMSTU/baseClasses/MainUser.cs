﻿using System;
using System.Collections.Generic;
using VkNet.Model.RequestParams;
using VkNet.Model.Keyboard;
using VkNet.Enums.SafetyEnums;
using System.Text.RegularExpressions;

namespace BotBMSTU
{
    public abstract class MainUser
    {
        protected List<Command> commands;
        protected List<Thread> threads;
        protected BotBMSTUDataSet database;

        public MainUser()
        {
            commands = CreateCommands();
            threads = new List<Thread>();
            database = new BotBMSTUDataSet();
        }

        protected abstract List<Command> CreateCommands();

        public List<MessagesSendParams> PerformCommand(VkNet.Model.Message message)
        {
            foreach (Thread thread in threads)
            {
                if (thread.id == message.FromId) return thread.command.cb(message, thread);
            }
            foreach (Command command in commands)
            {
                if (command.name.ToLower() == message.Text.ToLower())
                {
                    Thread thread = new Thread(command, message.FromId);
                    threads.Add(thread);
                    return command.cb(message, thread);
                }
            }
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            if (message.Text != Bot.password)
            {
                messages.Add(new MessagesSendParams
                {
                    UserId = message.FromId,
                    Message = "Команда не найдена.\n\n" + CommandsToListString(),
                    Attachments = null,
                    Keyboard = CreateKeyboard(CommandsToList())
                });
            } 
            else messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = CommandsToListString(),
                Attachments = null,
                Keyboard = CreateKeyboard(CommandsToList())
            });
            return messages;
        }

        protected MessageKeyboard CreateKeyboard(List<string> buttons)
        {
            KeyboardBuilder kb = new KeyboardBuilder(true);
            if (buttons.Count > 40) return null;
            else
            {
                int rowCount = buttons.Count / 10;
                if (buttons.Count % 10 != 0) rowCount++;
                for (int i = 0; i < buttons.Count; i++)
                {
                    if (i != 0 && i % rowCount == 0) kb.AddLine();
                    if (buttons[i].ToLower() == "начать" || buttons[i].ToLower() == "да") kb.AddButton(buttons[i], null, KeyboardButtonColor.Positive, "text");
                    else if (buttons[i].ToLower() == "стоп" || buttons[i].ToLower() == "нет" || buttons[i].ToLower() == "отменить") kb.AddButton(buttons[i], null, KeyboardButtonColor.Negative, "text");
                    else kb.AddButton(buttons[i], null);
                }
            }
            return kb.Build();
        }
        protected List<MessagesSendParams> GetListOfCommands(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = CommandsToListString(),
                Attachments = null,
                Keyboard = CreateKeyboard(CommandsToList())
            }); ;
            threads.Remove(thread);
            return messages;
        }

        protected List<string> CommandsToList()
        {
            List<string> result = new List<string>();
            foreach(var command in commands)
            {
                result.Add(command.name);
            }
            return result;
        }

        protected string CommandsToListString()
        {
            string result = "Список команд:\n\n";
            for (int i = 0; i < commands.Count; i++) result += (i + 1).ToString() + ") " + commands[i].name + " - " + commands[i].description + ";\n";
            return result;
        }

        protected bool IsNumber(string num)
        {
            try
            {
                int result = Convert.ToInt32(num);
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected void DeleteThreadByUserId(long? id)
        {
            foreach(Thread t in threads)
            {
                if (t.id == id) threads.Remove(t);
            }
        }

        protected Thread FindThreadById(long? id)
        {
            foreach(Thread t in threads)
            {
                if (t.id == id) return t;
            }
            return null;
        }

        protected bool IsRusText(string text)
        {
            Regex r = new Regex(@"[А-Яа-я]+");
            text = text.Replace(" ", "").Replace(",", "").Replace(".", "").Replace("-", "");
            text = text.Remove(r.Match(text).Index, r.Match(text).Value.Length);
            if (text.Length == 0) return true;
            return false;
        }
    }
}