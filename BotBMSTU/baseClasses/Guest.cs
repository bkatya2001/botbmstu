﻿using System;
using System.Collections.Generic;
using VkNet.Model.RequestParams;
using VkNet.Model.Keyboard;

namespace BotBMSTU
{
    class Guest : MainUser
    {
        public Guest() : base() { }

        protected override List<Command> CreateCommands()
        {
            List<Command> commands = new List<Command>();
            commands.Add(new Command("Регистрация", "Расскажите о себе, чтобы мы смогли подобрать Вам пару", SignUp));
            commands.Add(new Command("Список команд", "Изучите, чем я могу Вам помочь", GetListOfCommands));
            return commands;
        }

        public List<MessagesSendParams> SignUp(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            string answer = "Something went wrong";
            MessageKeyboard keyboard = null;
            List<string> parametr;
            switch (thread.step)
            {
                case 0:
                    answer = "Введите Вашу фамилию";
                    thread.step++;
                    break;
                case 1:
                    if (IsRusText(message.Text))
                    {
                        answer = "Введите Ваше имя";
                        thread.parametr = new List<string> { message.Text.ToLower() };
                        thread.step++;
                        thread.RestoreLives();
                    }
                    else
                    {
                        if (thread.HasLives()) answer = "Фамилия может состоять только из букв русского алфавита. Повторите ввод";
                        else
                        {
                            threads.Remove(thread);
                            answer = "Начните команду с самого начала";
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 2:
                    if (IsRusText(message.Text))
                    {
                        answer = "Укажите Ваш пол: 'Ж' - женский, 'М' - мужской";
                        parametr = (List<string>)thread.parametr;
                        keyboard = CreateKeyboard(new List<string> { "Ж", "М" });
                        parametr.Add(message.Text.ToLower());
                        thread.parametr = parametr;
                        thread.step++;
                        thread.RestoreLives();
                    }
                    else
                    {
                        if (thread.HasLives()) answer = "Имя может состоять только из букв русского алфавита. Повторите ввод";
                        else
                        {
                            threads.Remove(thread);
                            answer = "Начните команду с самого начала";
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 3:
                    parametr = (List<string>)thread.parametr;
                    if (message.Text.ToLower() == "ж" || message.Text.ToLower() == "м")
                    {
                        parametr.Add(message.Text.ToLower());
                        thread.parametr = parametr;
                        thread.step++;
                        thread.RestoreLives();
                        answer = "Укажите Ваш год рождения (введите число)";
                    }
                    else
                    {
                        if (thread.HasLives()) answer = "Неправильный формат. Повторите снова";
                        else
                        {
                            answer = "Повторите команду с самого начала";
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 4:
                    parametr = (List<string>)thread.parametr;
                    if (IsNumber(message.Text))
                    {
                        int age = Convert.ToInt32(message.Text);
                        if (age >= 1920 && age <= DateTime.Now.Year)
                        {
                            answer = "Перечислите Ваши увлечения через запятую";
                            parametr.Add(message.Text.ToLower());
                            thread.parametr = parametr;
                            thread.RestoreLives();
                            thread.step++;
                        }
                        else
                        {
                            if (thread.HasLives()) answer = "Вы не можете быть такого года рождения";
                            else
                            {
                                answer = "Повторите команду с самого начала";
                                threads.Remove(thread);
                                keyboard = CreateKeyboard(CommandsToList());
                            }
                        }
                    }
                    else
                    {
                        if (thread.HasLives()) answer = "Неправильный формат. Повторите снова";
                        else
                        {
                            answer = "Повторите команду с самого начала";
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 5:
                    if (IsRusText(message.Text))
                    {
                        parametr = (List<string>)thread.parametr;
                        parametr.Add(message.Text.ToLower());
                        thread.parametr = parametr;
                        thread.step++;
                        answer = "Укажите город, в котором Вы живёте (именительный падеж)";
                        thread.RestoreLives();
                    }
                    else
                    {
                        if (thread.HasLives()) answer = "Неправильный формат. Повторите снова";
                        else
                        {
                            answer = "Повторите команду с самого начала";
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 6:
                    if (IsRusText(message.Text))
                    {
                        parametr = (List<string>)thread.parametr;
                        parametr.Add(message.Text.ToLower());
                        thread.parametr = parametr;
                        thread.step++;
                        answer = "Прикрепите свою фотографию следующим сообщением";
                        keyboard = CreateKeyboard(new List<string> { "Без фото" });
                    }
                    else
                    {
                        if (thread.HasLives()) answer = "Город может состоять только из букв русского алфавита. Повторите ввод";
                        else
                        {
                            threads.Remove(thread);
                            answer = "Начните команду с самого начала";
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 7:
                    parametr = (List<string>)thread.parametr;
                    parametr.Add(message.Text.ToLower());
                    database.AddUser(message.FromId, parametr[0], parametr[1], parametr[5], Convert.ToInt32(parametr[3]), parametr[2], message);
                    database.AddHobbies(message.FromId, parametr[4], parametr[2]);
                    answer = "Регистрация завершена! Ознакомьтесь с новыми командами";
                    keyboard = CreateKeyboard(new List<string> { "Список команд" });
                    threads.Remove(thread);
                    break;
            }
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = answer,
                Attachments = null,
                Keyboard = keyboard
            });
            return messages;
        }
    }
}