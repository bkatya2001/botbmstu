﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using VkNet.Model.RequestParams;
using VkNet.Model.Keyboard;

namespace BotBMSTU
{
    class User : MainUser
    {
        public User() : base() { }

        protected override List<Command> CreateCommands()
        {
            List<Command> commands = new List<Command>
            {
                new Command("Мой профиль", "Убедитесь, что информация о Вас корректна", SeeProfile),
                new Command("Указать требования", "Опишите, что для Вас важно в человеке", AddRequests),
                new Command("Подобрать кандидата", "Попытайтесь отыскать свою судьбу с нашей помощью", SelectCandidate),
                new Command("Редактировать профиль", "Внесите изменения в информацию о Вас", EditInfo),
                new Command("События", "Узнайте, какие мероприятия организованы для Вас", GetEvents),
                new Command("Создать пару", "Поделитесь своим счастьем с нами", CreatePair),
                new Command("Разрушить пару", "Дайте знать, что Вам снова нужна наша помощь", DeletePair),
                new Command("Список команд", "Изучите, чем я могу Вам помочь", GetListOfCommands),
                new Command("Удалить профиль", "Удалите профиль, если услуги нашей компании Вам больше не требуются", DeleteProfile),
            };
            return commands;
        }

        public List<MessagesSendParams> SeeProfile(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            List<string> data = database.GetInformationById(message.FromId, database.GetSexById(message.FromId));
            string text = "Информация не найдена";
            if (data.Count != 0) text = "Ваша фамилия: " + data[0] + "\nВаше имя: " + data[1] + "\nВаш год рождения: " + data[2] + "\nВаш город проживания: " + data[3];
            text += "\nВаш id: " + message.FromId.ToString();
            string sex = database.GetSexById(message.FromId);
            List<string> requests = database.GetHobbyRequestsById(message.FromId, sex);
            if (requests.Count != 0)
            {
                text += "\nВаши требования к кандидату:\n";
                text += "Хобби: ";
                foreach (string r in requests) text += r + ", ";
                text = text.Remove(text.Length - 2);
            }
            requests = database.GetCityRequestsById(message.FromId, sex);
            if (requests.Count != 0)
            {
                text += "\nГорода: ";
                foreach (string r in requests) text += r + ", ";
                text = text.Remove(text.Length - 2);
                text += "\nВозраст: " + database.GetAgeRangeById(message.FromId, sex);
            }
            if (data.Count == 6)
            {
                string path = database.GetPhotoPathById(message.FromId, data[4]);
                WebClient webClient = new WebClient();
                var uploadServer = Bot.vkApi.Photo.GetMessagesUploadServer((long)message.PeerId);
                var responseFile = Encoding.ASCII.GetString(webClient.UploadFile(uploadServer.UploadUrl, path));
                var photo = Bot.vkApi.Photo.SaveMessagesPhoto(responseFile);
                messages.Add(new MessagesSendParams
                {
                    UserId = message.FromId,
                    Message = text + "\nВаше фото:",
                    Attachments = photo,
                    Keyboard = CreateKeyboard(CommandsToList())
            });
            } 
            else messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = text + "\nФото отсутствует",
                Attachments = null,
                Keyboard = CreateKeyboard(CommandsToList())
        });
            threads.Remove(thread);
            return messages;
        }

        private List<MessagesSendParams> DeleteProfile(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            string text = "Something went wrong";
            MessageKeyboard keyboard = null;
            switch (thread.step)
            {
                case 0:
                    text = "Вы действительно хотите удалить свой профиль? Напишите 'Да' или 'Нет'";
                    keyboard = CreateKeyboard(new List<string> { "Да", "Нет" });
                    thread.step++;
                    break;
                case 1:
                    switch (message.Text.ToLower())
                    {
                        case "да":
                            database.DeleteUserById(message.FromId);
                            text = "Профиль удалён";
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(new List<string> { "Список команд" });
                            break;
                        case "нет":
                            text = "Команда отменена";
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                            break;
                        default:
                            if (thread.HasLives())
                            {
                                text = "Я Вас не понял. Повторите ввод";
                                keyboard = CreateKeyboard(new List<string> { "Да", "Нет" });
                            }
                            else
                            {
                                text = "Начните команду сначала";
                                threads.Remove(thread);
                                keyboard = CreateKeyboard(CommandsToList());
                            }
                            break;
                    }
                    break;
            }
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = text,
                Attachments = null,
                Keyboard = keyboard
            });
            return messages;
        }

        private List<MessagesSendParams> AddRequests(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            string answer = "Something went wrong";
            MessageKeyboard keyboard = null;
            switch (thread.step)
            {
                case 0:
                    if ((database.GetCityRequestsById(message.FromId, database.GetSexById(message.FromId))).Count == 0)
                    {
                        answer = "Введите возрастной диапазон для Вашего кандидата.\nПример: 18-25";
                        thread.step++;
                        thread.parametr = database.GetSexById(message.FromId);
                    }
                    else
                    {
                        answer = "Вы уже указали требования. Для их редактирования напишите мне 'Редактировать профиль'";
                        keyboard = CreateKeyboard(new List<string> { "Редактировать профиль", "Список команд" });
                        threads.Remove(thread);
                    }
                    break;
                case 1:
                    try
                    {
                        string[] age = message.Text.Split('-');
                        if (IsNumber(age[0]) && IsNumber(age[1]) && Convert.ToInt32(age[1]) - Convert.ToInt32(age[0]) >= 0)
                        {
                            database.AddAgeRequests(DateTime.Now.Year - Convert.ToInt32(age[0]), DateTime.Now.Year - Convert.ToInt32(age[1]), thread.parametr.ToString(), message.FromId);
                            answer = "Перечислите через запятую увлечения Вашего кандидата. Если нет конктретных пожеланий, напишите 'Любое'";
                            thread.step++;
                            thread.RestoreLives();
                            keyboard = CreateKeyboard(new List<string> { "Любое" });
                        }
                        else
                        {
                            if (thread.HasLives()) answer = "Неправильный формат. Повторите снова";
                            else
                            {
                                threads.Remove(thread);
                                answer = "Начните команду с самого начала";
                                keyboard = CreateKeyboard(CommandsToList());
                            }
                        }
                    }
                    catch
                    {
                        if (thread.HasLives()) answer = "Неправильный формат. Повторите снова";
                        else
                        {
                            threads.Remove(thread);
                            answer = "Начните команду с самого начала";
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 2:
                    if (IsRusText(message.Text))
                    {
                        string[] hobbies = message.Text.ToLower().Split(',');
                        database.AddHobbyRequests(hobbies, thread.parametr.ToString(), message.FromId);
                        answer = "Укажите через запятую города, в которых может проживать Ваш кандидат. Если нет особых пожеланий, напишите 'Любой'";
                        thread.step++;
                        thread.RestoreLives();
                        keyboard = CreateKeyboard(new List<string> { "Любой" });
                    }
                    else
                    {
                        if (thread.HasLives()) answer = "Неправильный формат. Повторите снова";
                        else
                        {
                            threads.Remove(thread);
                            answer = "Начните команду с самого начала";
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 3:
                    if (IsRusText(message.Text))
                    {
                        string[] cities = message.Text.ToLower().Split(',');
                        database.AddCityRequests(cities, thread.parametr.ToString(), message.FromId);
                        answer = "Теперь мы сможем подобрать для Вас пару!";
                        keyboard = CreateKeyboard(CommandsToList());
                        threads.Remove(thread);
                    }
                    else
                    {
                        if (thread.HasLives()) answer = "Неправильный формат. Повторите снова";
                        else
                        {
                            threads.Remove(thread);
                            answer = "Начните команду с самого начала";
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
            }
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = answer,
                Attachments = null,
                Keyboard = keyboard
        });
            return messages;
        }

        private List<MessagesSendParams> EditInfo(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            string sex = database.GetSexById(message.FromId);
            MessageKeyboard keyboard = null;
            string text = "Something went wrong";
            switch (thread.step)
            {
                case 0:
                    text = "Что Вы хотите изменить?\nФамилия\nИмя\nГод рождения\nГород\nУвлечения\nТребования\nФото\n\nДля отмены напишите 'Отменить'";
                    thread.step++;
                    keyboard = CreateKeyboard(new List<string> { "Фамилия", "Имя", "Год рождения", "Город", "Увлечения", "Требования", "Фото", "Отменить" });
                    break;
                case 1:
                    text = "Введите новое значение";
                    thread.step++;
                    switch (message.Text.ToLower())
                    {
                        case "отменить":
                            text = "Команда отменена";
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                            break;
                        case "фамилия":
                            thread.parametr = 0;
                            thread.RestoreLives();
                            break;
                        case "имя":
                            thread.parametr = 1;
                            thread.RestoreLives();
                            break;
                        case "год рождения":
                            thread.parametr = 2;
                            thread.RestoreLives();
                            break;
                        case "город":
                            thread.parametr = 3;
                            thread.RestoreLives();
                            break;
                        case "фото":
                            thread.parametr = 4;
                            thread.RestoreLives();
                            text = "Пришлите новое фото следующим сообщением";
                            keyboard = CreateKeyboard(new List<string> { "Без фото" });
                            break;
                        case "увлечения":
                            thread.step = 3;
                            text = "Введите хобби через запятую";
                            break;
                        case "требования":
                            thread.RestoreLives();
                            text = "Что Вы хотите изменить?\nВозраста\nГорода\nУвлечения";
                            keyboard = CreateKeyboard(new List<string> { "Возраста", "Города", "Увлечения" });
                            thread.step = 4;
                            break;
                        default:
                            if (thread.HasLives())
                            {
                                text = "Некорректный ввод. Попробуйте снова";
                                thread.step--;
                            }
                            else
                            {
                                threads.Remove(thread);
                                text = "Начните команду с самого начала";
                                keyboard = CreateKeyboard(CommandsToList());
                            }
                            break;
                    }
                    break;
                case 2:
                    if (IsNumber(message.Text) && Convert.ToInt32(thread.parametr) == 2 && Convert.ToInt32(message.Text) >= 1920 && Convert.ToInt32(message.Text) <= DateTime.Now.Year || Convert.ToInt32(thread.parametr) != 2 && IsRusText(message.Text))
                    {
                        database.UpdateUserById(Convert.ToInt32(thread.parametr), message.Text.ToLower(), message.FromId, sex, message);
                        text = "Изменения добавлены";
                        keyboard = CreateKeyboard(CommandsToList());
                        threads.Remove(thread);
                    }
                    else
                    {
                        if (thread.HasLives()) text = "Неправильный формат. Повторите ввод";
                        else
                        {
                            threads.Remove(thread);
                            text = "Начните команду с самого начала";
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 3:
                    if (IsRusText(message.Text))
                    {
                        database.DeleteHobbyById(message.FromId, sex);
                        database.AddHobbies(message.FromId, message.Text.ToLower(), sex);
                        text = "Изменения добавлены";
                        keyboard = CreateKeyboard(CommandsToList());
                        threads.Remove(thread);
                    }
                    else
                    {
                        if (thread.HasLives()) text = "Неправильный формат. Повторите ввод";
                        else
                        {
                            threads.Remove(thread);
                            text = "Начните команду с самого начала";
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 4:
                    switch (message.Text.ToLower())
                    {
                        case "возраста":
                            text = "Введите диапазон возрастов. Пример: 18-25";
                            thread.step = 5;
                            thread.RestoreLives();
                            break;
                        case "города":
                            text = "Перечислите города через запятую";
                            thread.step = 6;
                            break;
                        case "увлечения":
                            text = "Перечислите хобби через запятую";
                            thread.step = 7;
                            break;
                        default:
                            if (thread.HasLives()) text = "Я Вас не понял. Повторите ввод";
                            else
                            {
                                threads.Remove(thread);
                                text = "Начните команду с самого начала";
                                keyboard = CreateKeyboard(CommandsToList());
                            }
                            break;
                    }
                    break;
                case 5:
                    string[] age = message.Text.ToLower(). Split('-');
                    if (IsNumber(age[0]) && IsNumber(age[1]) && Convert.ToInt32(age[1]) - Convert.ToInt32(age[0]) >= 0)
                    {
                        database.DeleteAgeRequestsById(message.FromId, sex);
                        database.AddAgeRequests(DateTime.Now.Year - Convert.ToInt32(age[0]), DateTime.Now.Year - Convert.ToInt32(age[1]), sex, message.FromId);
                        text = "Изменения добавлены";
                        keyboard = CreateKeyboard(CommandsToList());
                        threads.Remove(thread);
                    }
                    else
                    {
                        if (thread.HasLives()) text = "Неправильный формат. Попробуйте снова";
                        else
                        {
                            threads.Remove(thread);
                            text = "Начните команду с самого начала";
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 6:
                    if (IsRusText(message.Text))
                    {
                        string[] cities = message.Text.ToLower().Trim().Split(',');
                        database.DeleteCityRequestsById(message.FromId, sex);
                        database.AddCityRequests(cities, sex, message.FromId);
                        text = "Изменения добавлены";
                        keyboard = CreateKeyboard(CommandsToList());
                        threads.Remove(thread);
                    }
                    else
                    {
                        if (thread.HasLives()) text = "Неправильный формат. Попробуйте снова";
                        else
                        {
                            threads.Remove(thread);
                            text = "Начните команду с самого начала";
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 7:
                    if (IsRusText(message.Text))
                    {
                        string[] hobbies = message.Text.ToLower().Trim().Split(',');
                        database.DeleteHobbyRequestsById(message.FromId, sex);
                        database.AddHobbyRequests(hobbies, sex, message.FromId);
                        text = "Изменения добавлены";
                        keyboard = CreateKeyboard(CommandsToList());
                        threads.Remove(thread);
                    }
                    else
                    {
                        if (thread.HasLives()) text = "Неправильный формат. Попробуйте снова";
                        else
                        {
                            threads.Remove(thread);
                            text = "Начните команду с самого начала";
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
            }
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = text,
                Attachments = null,
                Keyboard = keyboard
            });
            return messages;
        }

        private List<MessagesSendParams> GetEvents(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            List<string> events = database.GetEvents();
            string text = "Something went wrong";
            if (events.Count > 0)
            {
                text = "На данный момент Вы можете принять участие в следующих мероприятиях\n\n";
                foreach (string e in events)
                {
                    text += e + "\n";
                }
            }
            else text = "На данный момент мероприятий незапланированно";
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = text,
                Attachments = null,
                Keyboard = CreateKeyboard(CommandsToList())
        });
            threads.Remove(thread);
            return messages;
        }

        private List<MessagesSendParams> CreatePair(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            string text = "Something went wrong";
            MessageKeyboard keyboard = null;
            switch (thread.step)
            {
                case 0:
                    if ((database.SelectIdInPair(message.FromId)).Count > 0)
                    {
                        text = "У Вас уже есть пара";
                        keyboard = CreateKeyboard(CommandsToList());
                        threads.Remove(thread);
                    }
                    else
                    {
                        text = "Введите id Вашей 'второй половинки'. Для отмены команды напишите 'Отмена'";
                        thread.step++;
                        keyboard = CreateKeyboard(new List<string> { "Отмена" });
                    }
                    break;
                case 1:
                    if (IsNumber(message.Text))
                    {
                        if (database.IsUser(Convert.ToInt32(message.Text)))
                        {
                            if ((database.SelectIdInPair(Convert.ToInt32(message.Text))).Count == 0)
                            {
                                thread.parametr = message.FromId;
                                thread.step++;
                                text = "Ждём ответа от 'второй половинки'";
                                thread.thread = FindThreadById(Convert.ToInt32(message.Text));
                                DeleteThreadByUserId(Convert.ToInt32(message.Text));
                                thread.id = Convert.ToInt32(message.Text);
                                thread.RestoreLives();
                                messages.Add(new MessagesSendParams
                                {
                                    UserId = Convert.ToInt32(message.Text),
                                    Message = "Пользователь " + database.GetNameSurnameById(message.FromId) + " хочет создать с Вами пару. Вы согласны? Пришлите 'Да' или 'Нет'. После Вы сможете продолжить работу",
                                    Attachments = null,
                                    Keyboard = CreateKeyboard(new List<string> { "Да", "Нет" })
                                });
                            } else
                            {
                                text = "У этого пользователя уже есть пара";
                                threads.Remove(thread);
                                keyboard = CreateKeyboard(CommandsToList());
                            }
                        }
                        else
                        {
                            text = "Пользователя с таким id нет в базе";
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    else
                    {
                        if (message.Text.ToLower() == "отмена")
                        {
                            text = "Команда отменена";
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                        else
                        {
                            if (thread.HasLives()) text = "Неправильный формат. Повторите снова";
                            else
                            {
                                threads.Remove(thread);
                                text = "Начните команду с самого начала";
                                keyboard = CreateKeyboard(CommandsToList());
                            }
                        }
                    }
                    break;
                case 2:
                    switch (message.Text.ToLower())
                    {
                        case "да":
                            text = database.AddPair(message.FromId, Convert.ToInt32(thread.parametr));
                            if (thread.thread != null) threads.Add(thread.thread);
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                            database.ChangeStatusById(message.FromId, true, database.GetSexById(message.FromId));
                            database.ChangeStatusById(Convert.ToInt32(thread.parametr), true, database.GetSexById(Convert.ToInt32(thread.parametr)));
                            messages.Add(new MessagesSendParams
                            {
                                UserId = Convert.ToInt32(thread.parametr),
                                Message = text,
                                Attachments = null,
                                Keyboard = keyboard
                            });
                            break;
                        case "нет":
                            text = "Очень жаль";
                            if (thread.thread != null) threads.Add(thread.thread);
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                            messages.Add(new MessagesSendParams
                            {
                                UserId = Convert.ToInt32(thread.parametr),
                                Message = "К сожалению, ответ оказался отрицательным...",
                                Attachments = null,
                                Keyboard = keyboard
                            });
                            break;
                        default:
                            if (thread.HasLives()) text = "Неправильный формат. Попробуйте ещё раз";
                            else
                            {
                                threads.Remove(thread);
                                text = "Начните команду с самого начала";
                                keyboard = CreateKeyboard(CommandsToList());
                            }
                            break;
                    }
                    break;
            }
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = text,
                Attachments = null,
                Keyboard = keyboard
            });
            return messages;
        }

        private List<MessagesSendParams> DeletePair(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            MessageKeyboard keyboard = null;
            string text = "Something went wrong";
            List<string> id = database.SelectIdInPair(message.FromId);
            switch (thread.step)
            {
                case 0:
                    if (id.Count > 0)
                    {
                        thread.parametr = id;
                        keyboard = CreateKeyboard(new List<string> { "Да", "Нет" });
                        text = "Вы действительно хотите разрушить пару? Напишите 'Да' или 'Нет'";
                        thread.step++;
                    }
                    else
                    {
                        text = "Вы не состоите в паре";
                        threads.Remove(thread);
                        keyboard = CreateKeyboard(CommandsToList());
                    }
                    messages.Add(new MessagesSendParams
                    {
                        UserId = message.FromId,
                        Message = text,
                        Keyboard = keyboard,
                        Attachments = null
                    });
                    break;
                case 1:
                    switch (message.Text.ToLower())
                    {
                        case "да":
                            database.DeletePairByMaleUserId(Convert.ToInt32(id[0]));
                            database.ChangeStatusById(database.GetVkId(Convert.ToInt32(id[0]), "м"), false, "м");
                            database.ChangeStatusById(database.GetVkId(Convert.ToInt32(id[1]), "ж"), false, "ж");
                            messages.Add(new MessagesSendParams
                            {
                                UserId = database.GetVkId(Convert.ToInt32(id[0]), "м"),
                                Message = "У Вас больше нет пары",
                                Attachments = null,
                                Keyboard = CreateKeyboard(CommandsToList())
                            });
                            messages.Add(new MessagesSendParams
                            {
                                UserId = database.GetVkId(Convert.ToInt32(id[1]), "ж"),
                                Message = "У Вас больше нет пары",
                                Attachments = null,
                                Keyboard = CreateKeyboard(CommandsToList())
                            });
                            threads.Remove(thread);
                            break;
                        case "нет":
                            messages.Add(new MessagesSendParams
                            {
                                UserId = message.FromId,
                                Message = "Команда отменена",
                                Attachments = null,
                                Keyboard = CreateKeyboard(CommandsToList())
                            });
                            threads.Remove(thread);
                            break;
                        default:
                            if (thread.HasLives())
                            {
                                text = "Я Вас не понял. Повторите ввод";
                                keyboard = CreateKeyboard(new List<string> { "Да", "Нет" });
                            }
                            else
                            {
                                text = "Начните команду с самого начала";
                                threads.Remove(thread);
                                keyboard = CreateKeyboard(CommandsToList());
                            }
                            messages.Add(new MessagesSendParams
                            {
                                UserId = message.FromId,
                                Message = text,
                                Attachments = null,
                                Keyboard = keyboard
                            });
                            break;
                    }
                    break;
            }
            return messages;
        }

        private List<MessagesSendParams> SelectCandidate(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            System.Collections.ObjectModel.ReadOnlyCollection<VkNet.Model.Attachments.Photo> photo = null;
            string text = "Something went wrong";
            MessageKeyboard keyboard = null;
            switch (thread.step)
            {
                case 0:
                    if (database.GetHobbyRequestsById(message.FromId, database.GetSexById(message.FromId)).Count > 0)
                    {
                        text = "Вам последовательно будут предлагаться подходящие кандидаты. Если человек Вам понравился, напишите 'Познакомиться'. " +
                            "Если человек показался Вам неинтересным, напишите 'Следующий'. Если Вы хотите закончить подбор кандидатов, напишите 'Стоп'." +
                            "Для начала подбора напишите 'Начать'";
                        thread.step++;
                        keyboard = CreateKeyboard(new List<string> { "Начать", "Стоп" });
                        List<int>id = database.GetIdOfCandidates(message.FromId, database.GetSexById(message.FromId));
                        thread.parametr = id;
                        if (id.Count > 0) thread.other_id = id[0];
                    }
                    else
                    {
                        text = "Для начала укажите требования";
                        keyboard = CreateKeyboard(new List<string> { "Указать требования", "Список команд" });
                        threads.Remove(thread);
                    }
                    break;
                case 1:
                    List<int> candidates = (List<int>)thread.parametr;
                    Random r = new Random();
                    List<string>data;
                    switch (message.Text.ToLower())
                    {
                        case "познакомиться":
                            candidates.Remove((int)thread.other_id);
                            if (thread.other_id <= 0)
                            {
                                text = "Этот человек не оставил контактов для знакомства с ним. Для продолжения напишите 'Следующий'; для завершения напишите 'Стоп'";
                                keyboard = CreateKeyboard(new List<string> { "Следующий", "Стоп" });
                            }
                            else
                            {
                                thread.thread = FindThreadById(thread.other_id);
                                keyboard = CreateKeyboard(CommandsToList());
                                DeleteThreadByUserId(thread.other_id);
                                thread.id = thread.other_id;
                                thread.other_id = message.FromId;
                                data = database.GetInformationById(message.FromId, database.GetSexById(message.FromId));
                                photo = null;
                                if (data.Count != 0) text = "Фамилия: " + data[0] + "\nИмя: " + data[1] + "\nГод рождения: " + data[2] + "\nГород проживания: " + data[3];
                                if (data.Count == 6)
                                {
                                    string path = database.GetPhotoPathById(message.FromId, data[4]);
                                    WebClient webClient = new WebClient();
                                    var uploadServer = Bot.vkApi.Photo.GetMessagesUploadServer((long)message.PeerId);
                                    var responseFile = Encoding.ASCII.GetString(webClient.UploadFile(uploadServer.UploadUrl, path));
                                    photo = Bot.vkApi.Photo.SaveMessagesPhoto(responseFile);
                                }
                                messages.Add(new MessagesSendParams
                                {
                                    UserId = thread.id,
                                    Message = "C Вами хочет познакомиться этот человек:\n\n" + text + "\n\nЕсли Вы заинтересовались, напишите 'Начать общение', иначе - 'Отвергнуть'",
                                    Attachments = photo,
                                    Keyboard = CreateKeyboard(new List<string> { "Начать общение", "Отвергнуть" })
                                });
                                text = "Заявка на знакомство отправлена. Ожидайте ответа предполагаемого кандидата";
                                photo = null;
                            }
                            thread.parametr = candidates;
                            break;
                        case "начать общение":
                            keyboard = CreateKeyboard(CommandsToList());
                            messages.Add(new MessagesSendParams
                            {
                                UserId = thread.other_id,
                                Message = "Приятного знакомства!\n\n" + "@id" + message.FromId,
                                Attachments = null,
                                Keyboard = keyboard
                            });
                            text = "Приятного знакомства!\n\n" + "@id" + thread.other_id;
                            photo = null;
                            if (thread.thread != null) threads.Add(thread.thread);
                            threads.Remove(thread);
                            break;
                        case "стоп":
                            text = "Подбор кандидатов окончен";
                            keyboard = CreateKeyboard(CommandsToList());
                            threads.Remove(thread);
                            break;
                        case "следующий":
                            candidates.Remove((int)thread.other_id);
                            if (candidates.Count == 0)
                            {
                                text = "К сожалению, нам не удалось подобрать Вам подходящего кандидата";
                                keyboard = CreateKeyboard(CommandsToList());
                                threads.Remove(thread);
                            }
                            else
                            {
                                thread.other_id = candidates[r.Next(0, candidates.Count - 1)];
                                if (database.GetSexById(message.FromId) == "ж") data = database.GetInformationById(thread.other_id, "м");
                                else data = database.GetInformationById(thread.other_id, "ж");
                                photo = null;
                                if (data.Count != 0) text = "Фамилия: " + data[0] + "\nИмя: " + data[1] + "\nГод рождения: " + data[2] + "\nГород проживания: " + data[3];
                                if (data.Count == 6)
                                {
                                    string path = database.GetPhotoPathById(thread.other_id, data[4]);
                                    WebClient webClient = new WebClient();
                                    var uploadServer = Bot.vkApi.Photo.GetMessagesUploadServer((long)message.PeerId);
                                    var responseFile = Encoding.ASCII.GetString(webClient.UploadFile(uploadServer.UploadUrl, path));
                                    photo = Bot.vkApi.Photo.SaveMessagesPhoto(responseFile);
                                }
                                thread.parametr = candidates;
                                keyboard = CreateKeyboard(new List<string> { "Познакомиться", "Следующий", "Стоп" });
                            }
                            break;
                        case "начать":
                            if (candidates.Count == 0)
                            {
                                text = "К сожалению, нам не удалось подобрать Вам подходящего кандидата";
                                keyboard = CreateKeyboard(CommandsToList());
                                threads.Remove(thread);
                            }
                            else
                            {
                                if (database.GetSexById(message.FromId) == "ж") data = database.GetInformationById(thread.other_id, "м");
                                else data = database.GetInformationById(thread.other_id, "ж");
                                keyboard = CreateKeyboard(new List<string> { "Познакомиться", "Следующий", "Стоп" });
                                if (data.Count != 0) text = "Фамилия: " + data[0] + "\nИмя: " + data[1] + "\nГод рождения: " + data[2] + "\nГород проживания: " + data[3];
                                if (data.Count == 6)
                                {
                                    string path = database.GetPhotoPathById(thread.other_id, data[4]);
                                    WebClient webClient = new WebClient();
                                    var uploadServer = Bot.vkApi.Photo.GetMessagesUploadServer((long)message.PeerId);
                                    var responseFile = Encoding.ASCII.GetString(webClient.UploadFile(uploadServer.UploadUrl, path));
                                    photo = Bot.vkApi.Photo.SaveMessagesPhoto(responseFile);
                                }
                            }
                            break;
                        case "отвергнуть":
                            text = "Кандидат отвергнут";
                            thread.id = thread.other_id;
                            if (thread.thread != null) threads.Add(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                            messages.Add(new MessagesSendParams
                            {
                                UserId = thread.other_id,
                                Message = "К сожалению, Ваше предложение познакомиться не приняли. Чтобы продолжить, напишите 'Следующий'",
                                Attachments = null,
                                Keyboard = keyboard
                            });
                            thread.other_id = message.FromId;
                            break;
                        default:
                            if (thread.HasLives()) text = "Я Вас не понял. Повторите ввод";
                            else
                            {
                                threads.Remove(thread);
                                text = "Начните команду с самого начала";
                                keyboard = CreateKeyboard(CommandsToList());
                            }
                            break;
                    }
                    break;
            }
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = text,
                Attachments = photo,
                Keyboard = keyboard
            });
            return messages;
        }
    }
}