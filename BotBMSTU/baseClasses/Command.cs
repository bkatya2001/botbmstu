﻿using System;
using System.Collections.Generic;
using System.Text;
using VkNet.Model.RequestParams;

namespace BotBMSTU
{
    public delegate List<MessagesSendParams> callback(VkNet.Model.Message message, Thread thread);

    public class Command
    {
        public string name;
        public string description;
        public callback cb { get; }

        public Command(string _name, string _description, callback _cb)
        {
            name = _name;
            description = _description;
            cb = _cb;
        }
    }
}