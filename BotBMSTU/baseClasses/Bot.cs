﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using VkNet.Enums.Filters;
using VkNet.Model;
using VkNet.Model.RequestParams;

namespace BotBMSTU
{
    class Bot
    {
        public static VkNet.VkApi vkApi;
        LongPollServerResponse response;
        BackgroundWorker backgroundMessages;
        BotBMSTUDataSet database;
        Admin admin;
        User user;
        Guest guest;
        static public string password = "password";
        Random r = new Random();

        public Bot()
        {
            vkApi = new VkNet.VkApi();
            vkApi.RequestsPerSecond = 20;
            database = new BotBMSTUDataSet();
            backgroundMessages = new BackgroundWorker();
            backgroundMessages.DoWork += new DoWorkEventHandler(DoBackgroundWork);
            admin = new Admin();
            user = new User();
            guest = new Guest();
        }

        public void StartBot()
        {
            if (!vkApi.IsAuthorized)
            {
                Settings scope = Settings.Messages | Settings.Wall | Settings.Friends | Settings.Photos | Settings.Documents | Settings.Offline;      // Приложение имеет доступ к друзьям
                try
                {
                    vkApi.Authorize(new ApiAuthParams
                    {
                        Settings = scope,
                        AccessToken = "51c90eda4c95f2343ea557cc47fdd98246e52f22eb71a3f94efbd0bbec3a8ae2683b36c93942bc3cf6627"
                    });
                }
                catch { }
                response = vkApi.Messages.GetLongPollServer(needPts: true);
            }
            var conversations = vkApi.Messages.GetConversations(new GetConversationsParams
            {
                StartMessageId = null,
                Offset = 0,
                Count = 200,
                Filter = VkNet.Enums.SafetyEnums.GetConversationFilter.Unanswered
            }).Items;
            foreach(var c in conversations) performCommand(c.LastMessage);
            backgroundMessages.RunWorkerAsync();
        }

        void performCommand(VkNet.Model.Message message)
        {
            if (database.IsAdmin(message.FromId)) SendMessages(admin.PerformCommand(message));
            else if (database.IsUser(message.FromId)) SendMessages(user.PerformCommand(message));
            else if (message.Text == password)
            {
                database.Add(message.FromId);
                SendMessages(admin.PerformCommand(message));
            }
            else SendMessages(guest.PerformCommand(message));
        }

        private void SendMessages(List<MessagesSendParams> messages)
        {
            foreach (var m in messages)
            {
                try
                {
                    if (vkApi.RequestsPerSecond <= 10)
                    {
                        System.Threading.Thread.Sleep(1000);
                        vkApi.RequestsPerSecond = 20;
                    }
                    m.RandomId = r.Next();
                    vkApi.Messages.Send(m);
                    vkApi.RequestsPerSecond--;
                }
                catch (Exception e) { }
            }
        } 
        void DoBackgroundWork(object sender, DoWorkEventArgs e)
        {
            var resp = vkApi.Messages.GetLongPollHistory(new MessagesGetLongPollHistoryParams
            {
                Ts = Convert.ToUInt64(response.Ts),
                Pts = Convert.ToUInt64(response.Pts)
            });
            while (true)
            {
                try
                {
                    resp = vkApi.Messages.GetLongPollHistory(new MessagesGetLongPollHistoryParams
                    {
                        Pts = resp.NewPts
                    });
                    foreach (VkNet.Model.Message message in resp.Messages)
                    {
                        if (message.Type == VkNet.Enums.MessageType.Received)
                        {
                            performCommand(message);
                        }
                    }
                    if (backgroundMessages.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }
                    database.ClearBase();
                    System.Threading.Thread.Sleep(200);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("token")) vkApi.RequestsPerSecond = 20;

                }
            }
        }
    }
}