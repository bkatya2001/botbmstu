﻿namespace BotBMSTU
{
    public class Thread
    {
        public Command command;
        public int step;
        public long? id;
        public object parametr;
        public Thread thread;
        public long? other_id;
        private int lives;

        public Thread(Command _command, long? _id)
        {
            command = _command;
            id = _id;
            step = 0;
            parametr = new object();
            thread = null;
            other_id = 0;
            lives = 3;
        }

        public bool HasLives()
        {
            lives--;
            if (lives <= 0) return false;
            else return true;
        }

        public void RestoreLives()
        {
            lives = 3;
        }
    }
}