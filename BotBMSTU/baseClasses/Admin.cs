﻿using System;
using System.Collections.Generic;
using System.Text;
using VkNet.Model.RequestParams;
using System.Net;
using VkNet.Model.Keyboard;

namespace BotBMSTU
{
    class Admin : MainUser
    {
        public Admin() : base() { }

        protected override List<Command> CreateCommands()
        {
            List<Command> commands = new List<Command>
            {
                new Command("Статистика", "", GetStatistics),
                new Command("Посмотреть пользователя", "", SeeUser),
                new Command("Посмотреть пару", "", SeePair),
                new Command("Посмотреть мероприятие", "", SeeEvent),
                new Command("Добавить пользователя", "", AddUser),
                new Command("Добавить пару", "", AddPair),
                new Command("Добавить мероприятие", "", AddEvent),
                new Command("Редактировать пользователя", "", EditUser),
                new Command("Редактировать пару", "", EditPair),
                new Command("Редактировать мероприятие", "", EditEvent),
                new Command("Удалить пользователя", "", DeleteUser),
                new Command("Удалить пару", "", DeletePair),
                new Command("Удалить мероприятие", "", DeleteEvent),
                new Command("Заполнить базу", "", FillBase),
                new Command("Список команд", "", GetListOfCommands),
                new Command("Выйти", "", LogOut)
            };
            return commands;
        }

        private List<MessagesSendParams> LogOut(VkNet.Model.Message message, Thread thread)
        {
            database.DeleteAdminById(message.FromId);
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            threads.Remove(thread);
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = "С нетерпением жду Вашего возвращения!",
                Attachments = null,
                Keyboard = CreateKeyboard(new List<string> { "Список команд" })
            });
            return messages;
        }

        private List<MessagesSendParams> DeletePair(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            string text = "Something went wrong";
            MessageKeyboard keyboard = null;
            switch (thread.step)
            {
                case 0:
                    text = "Введите id страницы пользователя";
                    thread.step++;
                    break;
                case 1:
                    if (IsNumber(message.Text))
                    {
                        List<string> id = database.SelectIdInPair(Convert.ToInt32(message.Text));
                        database.DeletePairByMaleUserId(Convert.ToInt32(id[0]));
                        database.ChangeStatusById(database.GetVkId(Convert.ToInt32(id[0]), "м"), false, "м");
                        database.ChangeStatusById(database.GetVkId(Convert.ToInt32(id[1]), "ж"), false, "ж");
                        text = "Пара удалена";
                        keyboard = CreateKeyboard(CommandsToList());
                        threads.Remove(thread);
                    }
                    else
                    {
                        if (thread.HasLives()) text = "Неправильный формат id. Повторите ввод снова";
                        else
                        {
                            text = "Начните команду заново";
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
            }
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = text,
                Attachments = null,
                Keyboard = keyboard
            });
            return messages;
        }

        private List<MessagesSendParams> DeleteUser(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            string text = "Something went wrong";
            MessageKeyboard keyboard = null;
            switch (thread.step)
            {
                case 0:
                    text = "Введите id страницы пользователя";
                    thread.step++;
                    break;
                case 1:
                    if (IsNumber(message.Text))
                    {
                        text = "Пользователь удалён";
                        database.DeleteUserById(Convert.ToInt32(message.Text));
                        keyboard = CreateKeyboard(CommandsToList());
                        threads.Remove(thread);
                    }
                    else
                    {
                        if (thread.HasLives()) text = "Неправильный формат id";
                        else
                        {
                            text = "Повторите команду снова";
                            keyboard = CreateKeyboard(CommandsToList());
                            threads.Remove(thread);
                        }
                    }
                    break;
            }
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = text,
                Attachments = null,
                Keyboard = keyboard
            });
            return messages;
        }

        private List<MessagesSendParams> DeleteEvent(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            string text = "Something went wrong";
            switch (thread.step)
            {
                case 0:
                    text = "Введите название мероприятия";
                    thread.step++;
                    break;
                case 1:
                    text = "Мероприятие удалено";
                    database.DeleteEventByName(message.Text);
                    threads.Remove(thread);
                    break;
            }
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = text,
                Attachments = null,
                Keyboard = CreateKeyboard(CommandsToList())
            });
            return messages;
        }

        private List<MessagesSendParams> EditUser(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            string text = "Something went wrong";
            MessageKeyboard keyboard = null;
            List<int> parametr;
            switch (thread.step)
            {
                case 0:
                    text = "Введите id страницы пользователя";
                    thread.step++;                   
                    break;
                case 1:
                    if (IsNumber(message.Text))
                    {
                        text = "Что Вы хотите изменить?\nФамилия\nИмя\nГод рождения\nГород\nУвлечения\nТребования\nФото";
                        thread.step++;
                        thread.parametr = new List<int> { Convert.ToInt32(message.Text) };
                        keyboard = CreateKeyboard(new List<string> { "Фамилия", "Имя", "Год рождения", "Город", "Увлечения", "Требования", "Фото" });
                        thread.RestoreLives();
                    }
                    else
                    {
                        if (thread.HasLives()) text = "Неправильный формат. Повторите ввод снова";
                        else
                        {
                            threads.Remove(thread);
                            text = "Повторите команду заново";
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 2:
                    parametr = (List<int>)thread.parametr;
                    text = "Введите новое значение";
                    thread.step++;
                    switch (message.Text.ToLower())
                    {
                        case "фамилия":
                            parametr.Add(0);
                            thread.RestoreLives();
                            break;
                        case "имя":
                            parametr.Add(1);
                            thread.RestoreLives();
                            break;
                        case "год рождения":
                            parametr.Add(2);
                            thread.RestoreLives();
                            break;
                        case "город":
                            parametr.Add(3);
                            thread.RestoreLives();
                            break;
                        case "фото":
                            parametr.Add(4);
                            text = "Прикрепите новое фото к следующему сообщению";
                            thread.RestoreLives();
                            break;
                        case "увлечения":
                            thread.step = 4;
                            text = "Введите хобби через запятую";
                            break;
                        case "требования":
                            thread.step = 5;
                            text = "Что Вы хотите изменить?\nВозраста\nГорода\nУвлечения";
                            keyboard = CreateKeyboard(new List<string> { "Возраста", "Города", "Увлечения" });
                            thread.RestoreLives();
                            break;
                        default:
                            if (thread.HasLives())
                            {
                                text = "Некорректный ввод. Попробуйте снова";
                                thread.step--;
                                keyboard = CreateKeyboard(new List<string> { "Фамилия", "Имя", "Год рождения", "Город", "Увлечения", "Требования", "Фото" });
                            }
                            else
                            {
                                threads.Remove(thread);
                                text = "Начните команду заново";
                                keyboard = CreateKeyboard(CommandsToList());
                            }
                            break;
                    }
                    break;
                case 3:
                    parametr = (List<int>)thread.parametr;
                    if (IsNumber(message.Text) || parametr[1] != 2)
                    {
                        database.UpdateUserById(parametr[1], message.Text.ToLower(), parametr[0], database.GetSexById(parametr[0]), message);
                        text = "Изменения добавлены";
                        keyboard = CreateKeyboard(CommandsToList());
                        threads.Remove(thread);
                    }
                    else
                    {
                        if (thread.HasLives()) text = "Неправильный формат. Повторите снова";
                        else
                        {
                            threads.Remove(thread);
                            text = "Начните команду заново";
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 4:
                    parametr = (List<int>)thread.parametr;
                    database.DeleteHobbyById(parametr[0], database.GetSexById(parametr[0]));
                    database.AddHobbies(parametr[0], message.Text, database.GetSexById(parametr[0]));
                    text = "Изменения добавлены";
                    keyboard = CreateKeyboard(CommandsToList());
                    threads.Remove(thread);
                    break;
                case 5:
                    switch (message.Text.ToLower())
                    {
                        case "возраста":
                            text = "Введите диапазон возрастов. Пример: 18-25";
                            thread.step = 6;
                            thread.RestoreLives();
                            break;
                        case "города":
                            text = "Перечислите города через запятую";
                            thread.step = 7;
                            break;
                        case "увлечения":
                            text = "Перечислите хобби через запятую";
                            thread.step = 8;
                            break;
                        default:
                            if (thread.HasLives())
                            {
                                text = "Я Вас не понял, повторите ввод";
                                keyboard = CreateKeyboard(new List<string> { "Возраста", "Города", "Увлечения" });
                            }
                            else
                            {
                                threads.Remove(thread);
                                text = "Начните команду заново";
                                keyboard = CreateKeyboard(CommandsToList());
                            }
                            break;
                    }
                    break;
                case 6:
                    parametr = (List<int>)thread.parametr;
                    string[] age = message.Text.Split('-');
                    if (IsNumber(age[0]) && IsNumber(age[1]) && Convert.ToInt32(age[1]) - Convert.ToInt32(age[0]) >= 0)
                    {
                        database.DeleteAgeRequestsById(parametr[0], database.GetSexById(parametr[0]));
                        database.AddAgeRequests(DateTime.Now.Year - Convert.ToInt32(age[0]), DateTime.Now.Year - Convert.ToInt32(age[1]), database.GetSexById(parametr[0]), parametr[0]);
                        text = "Изменения добавлены";
                        keyboard = CreateKeyboard(CommandsToList());
                        threads.Remove(thread);
                    }
                    else
                    {
                        if (thread.HasLives()) text = "Неправильный формат. Попробуйте снова";
                        else
                        {
                            text = "Повторите команду с самого начала";
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 7:
                    parametr = (List<int>)thread.parametr;
                    string[] cities = message.Text.ToLower().Trim().Split(',');
                    database.DeleteCityRequestsById(parametr[0], database.GetSexById(parametr[0]));
                    database.AddCityRequests(cities, database.GetSexById(parametr[0]), parametr[0]);
                    text = "Изменения добавлены";
                    keyboard = CreateKeyboard(CommandsToList());
                    threads.Remove(thread);
                    break;
                case 8:
                    parametr = (List<int>)thread.parametr;
                    string[] hobbies = message.Text.ToLower().Trim().Split(',');
                    database.DeleteHobbyRequestsById(parametr[0], database.GetSexById(parametr[0]));
                    database.AddHobbyRequests(hobbies, database.GetSexById(parametr[0]), parametr[0]);
                    text = "Изменения добавлены";
                    keyboard = CreateKeyboard(CommandsToList());
                    threads.Remove(thread);
                    break;
            }
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = text,
                Attachments = null,
                Keyboard = keyboard
            });
            return messages;
        }

        private List<MessagesSendParams> EditPair(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            string text = "Something went wrong";
            MessageKeyboard keyboard = null;
            switch (thread.step)
            {
                case 0:
                    text = "Введите id страницы пользователя, которого нужно заменить";
                    thread.step++;
                    break;
                case 1:
                    if (IsNumber(message.Text))
                    {
                        text = "Введите новый id";
                        thread.parametr = message.Text;
                        thread.step++;
                        thread.RestoreLives();
                    }
                    else
                    {
                        if (thread.HasLives()) text = "Неправильный формат. Попробуйте снова";
                        else
                        {
                            text = "Повторите команду с самого начала";
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 2:
                    if (IsNumber(message.Text))
                    {
                        int newId = Convert.ToInt32(message.Text);
                        if (database.IsUser(newId))
                        {
                            int oldId = Convert.ToInt32(thread.parametr);
                            string sex = database.GetSexById(oldId);
                            if (sex == database.GetSexById(newId))
                            {
                                database.ChangePair(oldId, newId, sex, DateTime.Now.Date);
                                database.ChangeStatusById(oldId, false, sex);
                                database.ChangeStatusById(newId, true, sex);
                                text = "Изменения добавлены";
                                keyboard = CreateKeyboard(CommandsToList());
                                threads.Remove(thread);
                            }
                            else
                            {
                                if (thread.HasLives()) text = "Участники пары должны быть разного пола. Повторите ввод";
                                else
                                {
                                    text = "Повторите команду с самого начала";
                                    threads.Remove(thread);
                                    keyboard = CreateKeyboard(CommandsToList());
                                }
                            }
                        }
                        else
                        {
                            if (thread.HasLives()) text = "Мы не знаем пользователя с таким id. Попробуйте снова";
                            else
                            {
                                text = "Повторите команду с самого начала";
                                threads.Remove(thread);
                                keyboard = CreateKeyboard(CommandsToList());
                            }
                        }
                    }
                    else
                    {
                        if (thread.HasLives()) text = "Неправильный формат. Попробуйте снова";
                        else
                        {
                            text = "Повторите команду с самого начала";
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
            }
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = text,
                Attachments = null,
                Keyboard = keyboard
            });
            return messages;
        }

        private List<MessagesSendParams> EditEvent(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            string text = "Something went wrong";
            MessageKeyboard keyboard = null;
            switch (thread.step)
            {
                case 0:
                    text = "Введите название мероприятия, которое хотите изменить";
                    thread.step++;
                    break;
                case 1:
                    if (database.IsNameOfEvent(message.Text.ToLower()))
                    {
                        text = "Что Вы хотите изменить?\n\nНазвание\nОписание\nДату";
                        thread.step++;
                        thread.parametr = message.Text.ToLower();
                        keyboard = CreateKeyboard(new List<string> { "Название", "Описание", "Дату" });
                        thread.RestoreLives();
                    }
                    else
                    {
                        if (thread.HasLives()) text = "Такого мероприятия нет. Попробуйте снова";
                        else
                        {
                            text = "Повторите команду с самого начала";
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 2:
                    text = "Введите новое значение";
                    switch (message.Text.ToLower())
                    {
                        case "название":
                            thread.step = 3;
                            break;
                        case "описание":
                            thread.step = 4;
                            break;
                        case "дату":
                            text = "Введите дату и время в формате ДД.ММ.ГГГГ";
                            thread.step = 5;
                            break;
                        default:
                            if (thread.HasLives())
                            {
                                text = "Мы не поняли, что Вы хотите изменить. Попробуйте снова";
                                keyboard = CreateKeyboard(new List<string> { "Название", "Описание", "Дату" });
                            }
                            else
                            {
                                text = "Повторите команду с самого начала";
                                threads.Remove(thread);
                                keyboard = CreateKeyboard(CommandsToList());
                            }
                            break;
                    }
                    break;
                case 3:
                    database.EditEvent("название", message.Text.ToLower(), thread.parametr.ToString());
                    threads.Remove(thread);
                    text = "Изменения внесены";
                    keyboard = CreateKeyboard(CommandsToList());
                    break;
                case 4:
                    database.EditEvent("описание", message.Text.ToLower(), thread.parametr.ToString());
                    threads.Remove(thread);
                    text = "Изменения внесены";
                    keyboard = CreateKeyboard(CommandsToList());
                    break;
                case 5:
                    database.EditEvent("дату", message.Text, thread.parametr.ToString());
                    threads.Remove(thread);
                    text = "Изменения внесены";
                    keyboard = CreateKeyboard(CommandsToList());
                    break;
            }
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = text,
                Attachments = null,
                Keyboard = keyboard
            });
            return messages;
        }

        public List<MessagesSendParams> AddUser(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            MessageKeyboard keyboard = null;
            string answer = "Something went wrong";
            List<string> parametr;
            switch (thread.step)
            {
                case 0:
                    answer = "Введите id страницы пользователя";
                    thread.step++;
                    break;
                case 1:
                    if (IsNumber(message.Text))
                    {
                        thread.other_id = Convert.ToInt32(message.Text);
                        answer = "Введите фамилию";
                        thread.step++;
                        thread.RestoreLives();
                    }
                    else
                    {
                        if (thread.HasLives()) answer = "Неправильный формат. Повторите ввод снова";
                        else
                        {
                            answer = "Повторите команду с самого начала";
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 2:
                    answer = "Введите имя";
                    thread.parametr = new List<string> { message.Text.ToLower() };
                    thread.step++;
                    break;
                case 3:
                    answer = "Укажите пол: 'Ж' - женский, 'М' - мужской";
                    parametr = (List<string>)thread.parametr;
                    parametr.Add(message.Text.ToLower());
                    thread.parametr = parametr;
                    thread.step++;
                    keyboard = CreateKeyboard(new List<string> { "Ж", "М" });
                    break;
                case 4:
                    List<string> ts = (List<string>)thread.parametr;
                    if (message.Text.ToLower() == "ж" || message.Text.ToLower() == "м")
                    {
                        ts.Add(message.Text.ToLower());
                        thread.parametr = ts;
                        thread.step++;
                        answer = "Укажите год рождения (введите число)";
                        thread.RestoreLives();
                    }
                    else
                    {
                        if (thread.HasLives()) answer = "Неправильный формат. Повторите снова";
                        else
                        {
                            answer = "Повторите команду с самого начала";
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 5:
                    parametr = (List<string>)thread.parametr;
                    if (IsNumber(message.Text))
                    {
                        int age = Convert.ToInt32(message.Text);
                        answer = "Перечислите увлечения через запятую";
                        parametr.Add(message.Text.ToLower());
                        thread.parametr = parametr;
                        thread.step++;
                    }
                    else
                    {
                        if (thread.HasLives()) answer = "Неправильный формат. Повторите снова";
                        else
                        {
                            answer = "Повторите команду с самого начала";
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 6:
                    parametr = (List<string>)thread.parametr;
                    parametr.Add(message.Text.ToLower());
                    thread.parametr = parametr;
                    thread.step++;
                    answer = "Укажите город (именительный падеж)";
                    break;
                case 7:
                    List<string> ss = (List<string>)thread.parametr;
                    ss.Add(message.Text.ToLower());
                    thread.parametr = ss;
                    thread.step++;
                    answer = "Прикрепите фотографию следующим сообщением";
                    keyboard = CreateKeyboard(new List<string> { "Без фото" });
                    break;
                case 8:
                    List<string> shs = (List<string>)thread.parametr;
                    shs.Add(message.Text.ToLower());
                    database.AddUser(thread.other_id, shs[0], shs[1], shs[5], Convert.ToInt32(shs[3]), shs[2], message);
                    database.AddHobbies(thread.other_id, shs[4], shs[2]);
                    answer = "Пользователь добавлен";
                    keyboard = CreateKeyboard(CommandsToList());
                    threads.Remove(thread);
                    break;
            }
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = answer,
                Attachments = null,
                Keyboard = keyboard
            });
            return messages;
        }

        private List<MessagesSendParams> AddPair(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            string text = "Something went wrong";
            MessageKeyboard keyboard = null;
            switch (thread.step)
            {
                case 0:
                    text = "Введите id страницы мужчины";
                    thread.step++;
                    break;
                case 1:
                    if (IsNumber(message.Text))
                    {
                        if ("м" == database.GetSexById(Convert.ToInt32(message.Text)))
                        {
                            text = "Введите id страницы женщины";
                            thread.step++;
                            thread.parametr = message.Text;
                            thread.RestoreLives();
                        }
                        else
                        {
                            if (thread.HasLives()) text = "Это женщина. Введите id страницы мужчины";
                            else
                            {
                                text = "Повторите команду с самого начала";
                                threads.Remove(thread);
                                keyboard = CreateKeyboard(CommandsToList());
                            }
                        }
                    }
                    else
                    {
                        if (thread.HasLives()) text = "Неправильный формат. Повторите ввод снова";
                        else
                        {
                            text = "Повторите команду с самого начала";
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 2:
                    if (IsNumber(message.Text))
                    {
                        if ("ж" == database.GetSexById(Convert.ToInt32(message.Text)))
                        {
                            text = database.AddPair(Convert.ToInt32(thread.parametr), Convert.ToInt32(message.Text));
                            database.ChangeStatusById(Convert.ToInt32(message.Text), true, "ж");
                            database.ChangeStatusById(Convert.ToInt32(thread.parametr), true, "м");
                            keyboard = CreateKeyboard(CommandsToList());
                            threads.Remove(thread);
                        }
                        else
                        {
                            if (thread.HasLives()) text = "Это мужчина. Введите id страницы женщины";
                            else
                            {
                                text = "Повторите команду с самого начала";
                                threads.Remove(thread);
                                keyboard = CreateKeyboard(CommandsToList());
                            }
                        }
                    }
                    else
                    {
                        if (thread.HasLives()) text = "Неправильный формат. Повторите ввод снова";
                        else
                        {
                            text = "Повторите команду с самого начала";
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
            }
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = text,
                Attachments = null,
                Keyboard = keyboard
            });
            return messages;
        }

        private List<MessagesSendParams> AddEvent(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            List<string>parametr = new List<string>();
            string text = "Something went wrong";
            MessageKeyboard keyboard = null;
            switch (thread.step) 
            {
                case 0:
                    text = "Введите название мероприятия";
                    thread.step++;
                    break;
                case 1:
                    text = "Введите описание мероприятия";
                    thread.step++;
                    parametr.Add(message.Text);
                    thread.parametr = parametr;
                    break;
                case 2:
                    parametr = (List<string>)thread.parametr;
                    parametr.Add(message.Text);
                    thread.step++;
                    text = "Введите дату мероприятия";
                    break;
                case 3:
                    parametr = (List<string>)thread.parametr;
                    database.AddEvent(parametr[0], parametr[1], message.Text);
                    text = "Событие добавлено";
                    keyboard = CreateKeyboard(CommandsToList());
                    threads.Remove(thread);
                    break;
            }
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = text,
                Attachments = null,
                Keyboard = keyboard
            });
            return messages;
        }

        public List<MessagesSendParams> SeeUser(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            string text = "Something went wrong";
            MessagesSendParams m = new MessagesSendParams
            {
                UserId = message.FromId,
                Message = text,
                Attachments = null,
                Keyboard = null
        };
            switch (thread.step)
            {
                case 0:
                    text = "Введите id страницы пользователя";
                    thread.step++;
                    m.Message = text;
                    break;
                case 1:
                    if (IsNumber(message.Text))
                    {
                        int id = Convert.ToInt32(message.Text);
                        List<string> data = database.GetInformationById(id, database.GetSexById(id));
                        text = "Информация не найдена";
                        if (data.Count != 0) text = "Фамилия: " + data[0] + "\nИмя: " + data[1] + "\nГод рождения: " + data[2] + "\nГород проживания: " + data[3];
                        string sex = database.GetSexById(id);
                        List<string> requests = database.GetHobbyRequestsById(id, sex);
                        if (requests.Count != 0)
                        {
                            text += "\nТребования к кандидату:\n";
                            text += "Хобби: ";
                            foreach (string r in requests) text += r + ", ";
                            text = text.Remove(text.Length - 2);
                        }
                        requests = database.GetCityRequestsById(id, sex);
                        if (requests.Count != 0)
                        {
                            text += "\nГорода: ";
                            foreach (string r in requests) text += r + ", ";
                            text = text.Remove(text.Length - 2);
                            text += "\nВозраст: " + database.GetAgeRangeById(id, sex);
                        }
                        if (data.Count == 6)
                        {
                            string path = database.GetPhotoPathById(id, data[4]);
                            WebClient webClient = new WebClient();
                            var uploadServer = Bot.vkApi.Photo.GetMessagesUploadServer((long)message.PeerId);
                            var responseFile = Encoding.ASCII.GetString(webClient.UploadFile(uploadServer.UploadUrl, path));
                            var photo = Bot.vkApi.Photo.SaveMessagesPhoto(responseFile);
                            text += "\nФото:";
                            m.Attachments = photo;
                        }
                        else text += "\nФото отсутствует";
                        m.Message = text;
                        m.Keyboard = CreateKeyboard(CommandsToList());
                        threads.Remove(thread);
                    }
                    else
                    {
                        if (thread.HasLives()) m.Message = "Неправильный формат. Повторите ввод";
                        else
                        {
                            m.Message = "Повторите команду с самого начала";
                            threads.Remove(thread);
                            m.Keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
            }
            messages.Add(m);
            return messages;
        }

        private List<MessagesSendParams> SeePair(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            string text = "Something went wrong";
            MessageKeyboard keyboard = null;
            switch (thread.step)
            {
                case 0:
                    text = "Введите id страницы пользователя";
                    thread.step++;
                    break;
                case 1:
                    if (IsNumber(message.Text))
                    {
                        List<string> id = database.SelectIdInPair(Convert.ToInt32(message.Text));
                        if (id.Count > 0) text = database.GetNameSurnameById(database.GetVkId(Convert.ToInt32(id[0]), "м")) + " - " + database.GetNameSurnameById(database.GetVkId(Convert.ToInt32(id[1]), "ж"));
                        else text = "У этого пользователя пока нет пары";
                        keyboard = CreateKeyboard(CommandsToList());
                        threads.Remove(thread);
                    }
                    else
                    {
                        if (thread.HasLives()) text = "Неправильный формат. Повторите ввод снова";
                        else
                        {
                            text = "Повторите команду с самого начала";
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
            }
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = text,
                Attachments = null,
                Keyboard = keyboard
        });
            return messages;
        }

        private List<MessagesSendParams> SeeEvent(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            List<string> events = database.GetEvents();
            string text = "Список доступных мероприятий:\n";
            foreach (string e in events) text += "\n" + e;
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = text,
                Attachments = null,
                Keyboard = CreateKeyboard(CommandsToList())
        });
            threads.Remove(thread);
            return messages;
        }

        private List<MessagesSendParams> GetStatistics(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            string text = "Something went wrong";
            MessageKeyboard keyboard = null;
            switch (thread.step)
            {
                case 0:
                    text = "Какую статистику Вы хотите посмотреть:\nОбщую\nПо парам";
                    keyboard = CreateKeyboard(new List<string> { "Общую", "По парам" });
                    thread.step++;
                    break;
                case 1:
                    switch (message.Text.ToLower())
                    {
                        case "общую":
                            text = "На данный момент в системе зарегистрировались " + database.GetUserStatistics("м") + " мужчин, " +
                                database.GetUserStatistics("ж") + " женщин. Всего пар: " + database.GetPairStatistics();
                            keyboard = CreateKeyboard(CommandsToList());
                            threads.Remove(thread);
                            break;
                        case "по парам":
                            text = "Укажите период. Введите начальную дату";
                            thread.step++;
                            break;
                        default:
                            if(thread.HasLives()) text = "Ввод был осуществлён с ошибкой. Повторите снова";
                            else
                            {
                                text = "Повторите команду с самого начала";
                                threads.Remove(thread);
                                keyboard = CreateKeyboard(CommandsToList());
                            }
                            break;
                    }
                    break;
                case 2:
                    thread.parametr = message.Text;
                    text = "Введите конечную дату";
                    thread.step++;
                    break;
                case 3:
                    text = "За указанный период сформировалось " + database.GetPairStatistics(thread.parametr.ToString(), message.Text) + " пар";
                    keyboard = CreateKeyboard(CommandsToList());
                    threads.Remove(thread);
                    break;
            }
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = text,
                Attachments = null,
                Keyboard = keyboard
            });
            return messages;
        }

        List<MessagesSendParams> FillBase(VkNet.Model.Message message, Thread thread)
        {
            List<MessagesSendParams> messages = new List<MessagesSendParams>();
            string text = "Something went wrong";
            MessageKeyboard keyboard = null;
            switch (thread.step)
            {
                case 0:
                    text = "Введите пол ('Ж' или 'М')";
                    keyboard = CreateKeyboard(new List<string> { "Ж", "М" });
                    thread.step++;
                    break;
                case 1:
                    if (message.Text.ToLower() == "ж" || message.Text.ToLower() == "м")
                    {
                        thread.parametr = message.Text.ToLower();
                        text = "Введите количество";
                        thread.RestoreLives();
                        thread.step++;
                    }
                    else
                    {
                        if (thread.HasLives()) text = "Неправильный формат. Повторите снова";
                        else
                        {
                            text = "Повторите команду с самого начала";
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
                case 2:
                    if (IsNumber(message.Text))
                    {
                        database.FillUsersTables(thread.parametr.ToString(), Convert.ToInt32(message.Text));
                        threads.Remove(thread);
                        text = "Пользователи добавлены";
                        keyboard = CreateKeyboard(CommandsToList());
                    }
                    else
                    {
                        if (thread.HasLives()) text = "Неправильный формат. Повторите снова";
                        else
                        {
                            text = "Повторите команду с самого начала";
                            threads.Remove(thread);
                            keyboard = CreateKeyboard(CommandsToList());
                        }
                    }
                    break;
            }
            messages.Add(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = text,
                Attachments = null,
                Keyboard = keyboard
            });
            return messages;
        }
    }
}